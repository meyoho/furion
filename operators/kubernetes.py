from __future__ import absolute_import
import base64
import logging
import copy
from operators.resource import feature_clusterrolebinding, job_template, secret_type_bootstrap_token 
from operators.ake import AkeJobTemplate, AkeSecretTemplate
from urllib3.exceptions import MaxRetryError

from kubernetes.client import (Configuration, ApiClient, RbacAuthorizationV1Api, CustomObjectsApi,
                               CoreV1Api, VersionApi, V1ConfigMap, BatchV1Api,
                               V1Secret, V1ObjectMeta, V1Namespace, V1DeleteOptions)
from kubernetes.client.rest import ApiException

from app_v2.region.entities import NodeListData


logger = logging.getLogger(__name__)

# define call apiserver timeout.
TIMEOUT = 5


class KubernetesApiError(Exception):

    def __init__(self, reason):
        self.reason = reason

    def __str__(self):
        return self.reason


class KubernetesUnauthorized(KubernetesApiError):

    def __init__(self):
        self.reason = "Current user's certificate is invalid."


class KubernetesForbidden(KubernetesApiError):

    def __init__(self):
        self.reason = "Current user does not have permission to perform this action."


class KubernetesResNotFound(KubernetesApiError):

    def __init__(self):
        self.reason = "Specified resource not found."


class KubernetesManager(object):
    def __init__(self, endpoint, token):
        """Initialize a kubernetes api operator. """
        config = Configuration()
        config.verify_ssl = False
        config.api_key_prefix = {'authorization': 'Bearer'}
        config.api_key = {'authorization': token}
        config.host = endpoint
        self.client = ApiClient(configuration=config)
        self.core_client = CoreV1Api(api_client=self.client)
        self.rbac_client = RbacAuthorizationV1Api(api_client=self.client)
        self.job_client = BatchV1Api(api_client=self.client)
        self.crd_client = CustomObjectsApi(self.client)
        self.version_client = VersionApi(api_client=self.client)

    def get_version(self):
        res = self._call(self.version_client, "get_code")
        return res.git_version[1:]

    def list_nodes(self, query_params):
        res = self._call(self.core_client, "list_node", **query_params).to_dict()
        return NodeListData(data=res)

    def delete_node(self, node_name):
        delete_options = V1DeleteOptions()
        self._call(self.core_client, "delete_node", node_name, delete_options)

    def patch_node(self, node_name, data):
        self._call(self.core_client, "patch_node", node_name, data)
    
    def add_nodes(self, endpoint, registry, node_data, namespace, version):
        ake_args, secret = AkeJobTemplate.get_ake_job_args(endpoint, node_data)
        self.create_secret_bootstrap_token(secret)
        image_registry = AkeJobTemplate.get_ake_job_image(registry, version)
        body = copy.deepcopy(job_template)
        body["metadata"]["name"] = AkeJobTemplate.get_ake_job_name()
        # body["metadata"]["namespace"] = "kube-system"
        body["spec"]["template"]["spec"]["containers"][0]["image"] = image_registry
        body["spec"]["template"]["spec"]["containers"][0]["args"] = ake_args
        return self._call(self.job_client, "create_namespaced_job", "kube-system", body).to_dict()
    
    def create_namespace(self, namespace):
        body = {"metadata": {"name": namespace}}
        return self._call(self.core_client, "create_namespace", body).to_dict()
    
    def check_namespace(self, namespace):
        namespace_list = self._call(self.core_client, "list_namespace").to_dict()
        for n in namespace_list['items']:
            if n["metadata"]["name"] == namespace:
                return True
        return False

    def check_cluster_role_binding(self):
        try:
            self._call(self.rbac_client,
                       "read_cluster_role_binding",
                       feature_clusterrolebinding['metadata']['name'])
        except KubernetesResNotFound:
            return False
        return True

    def create_namespace_binding(self, namespace):
        feature_clusterrolebinding["subjects"][0]["namespace"] = namespace
        body = feature_clusterrolebinding
        return self._call(self.rbac_client, "create_cluster_role_binding", body).to_dict()

    def add_namespace_label(self, namespace, label):
        body = {"metadata": {"labels": label}}
        return self._call(self.core_client, "patch_namespace", namespace, body).to_dict()

    def check_namespace_label(self, namespace):
        body = self._call(self.core_client, "read_namespace", namespace)
        if body.metadata.labels is not None and "alauda-registered-region" in body.metadata.labels:
            return True, body.metadata.labels["alauda-registered-region"]
        return False, None

    def delete_namespace_label(self, namespace):
        body = self._call(self.core_client, "read_namespace", namespace)
        if body.metadata.labels is not None and "alauda-registered-region" in body.metadata.labels:
            del body.metadata.labels['alauda-registered-region']
        return self._call(self.core_client, "replace_namespace", namespace, body).to_dict()

    def get_configmap_data(self, namespace, name):
        try:
            body = self._call(self.core_client, "read_namespaced_config_map", name, namespace)
        except KubernetesResNotFound:
            return {}
        else:
            return body.data

    def create_cluster_secret(self, secret, namespace):
        return self._call(self.core_client, "create_namespaced_secret", namespace, secret).to_dict()

    def create_cluster(self, cluster, namespace):
        group, version = "clusterregistry.k8s.io", "v1alpha1"
        return self._call(self.crd_client, "create_namespaced_custom_object",
                          group, version, namespace, 'clusters', cluster)

    def list_clusters(self, namespace):
        group, version = "clusterregistry.k8s.io", "v1alpha1"
        return self._call(self.crd_client, "list_namespaced_custom_object",
                          group, version, namespace, "clusters")

    def get_cluster(self, namespace, name):
        group, version = "clusterregistry.k8s.io", "v1alpha1"
        return self._call(self.crd_client, "get_namespaced_custom_object",
                          group, version, namespace, 'clusters', name)

    def delete_cluster(self, namespace, name):
        delete_options = V1DeleteOptions()
        group, version = "clusterregistry.k8s.io", "v1alpha1"
        return self._call(self.crd_client, "delete_namespaced_custom_object",
                          group, version, namespace, "clusters", name, delete_options)

    def create_secret_bootstrap_token(self, secret):
        token_time = AkeSecretTemplate.get_ake_secret_token_time()
        secret_type_bootstrap_token["metadata"]["name"] = "bootstrap-token-" + secret["token_id"]
        secret_type_bootstrap_token["stringData"]["token-id"] = secret["token_id"]
        secret_type_bootstrap_token["stringData"]["token-secret"] = secret["token_secret"]
        secret_type_bootstrap_token["stringData"]["expiration"] = token_time
        body = secret_type_bootstrap_token
        return self._call(self.core_client, "create_namespaced_secret", "kube-system", body).to_dict()

    def get_secret_data(self, namespace, name):
        try:
            body = self._call(self.core_client, "read_namespaced_secret", name, namespace)
        except KubernetesResNotFound:
            return {}
        else:
            return self._decode_secret_data(body.data)

    def get_dockercfg_secret(self, namespace, name):
        try:
            body = self._call(self.core_client, "read_namespaced_secret", name, namespace)
        except KubernetesResNotFound:
            return None
        else:
            return body.data.get(".dockerconfigjson", None)

    def apply_configmap_data(self, namespace, name, data):
        self.assure_namespace_exist(namespace)
        try:
            self._call(self.core_client, "read_namespaced_config_map", name, namespace)
        except KubernetesResNotFound:
            self._call(self.core_client, "create_namespaced_config_map", namespace,
                       V1ConfigMap(metadata=V1ObjectMeta(name=name, namespace=namespace),
                                   kind="ConfigMap", data=data))
        else:
            self._call(self.core_client, "patch_namespaced_config_map", name, namespace,
                       V1ConfigMap(metadata=V1ObjectMeta(name=name, namespace=namespace),
                                   kind="ConfigMap", data=data))

    def apply_secret_data(self, namespace, name, data):
        self.assure_namespace_exist(namespace)
        try:
            self._call(self.core_client, "read_namespaced_secret", name, namespace)
        except KubernetesResNotFound:
            self._call(self.core_client, "create_namespaced_secret", namespace,
                       V1Secret(metadata=V1ObjectMeta(name=name, namespace=namespace),
                                kind="Secret", data=self._encode_secret_data(data)))
        else:
            self._call(self.core_client, "patch_namespaced_secret", name, namespace,
                       V1Secret(metadata=V1ObjectMeta(name=name, namespace=namespace),
                                kind="Secret", data=self._encode_secret_data(data)))

    def apply_dockercfg_secret(self, namespace, name, data):
        self.assure_namespace_exist(namespace)
        try:
            self._call(self.core_client, "read_namespaced_secret", name, namespace)
        except KubernetesResNotFound:
            self._call(self.core_client, "create_namespaced_secret", namespace,
                       V1Secret(metadata=V1ObjectMeta(name=name, namespace=namespace),
                                kind="Secret", type="kubernetes.io/dockerconfigjson",
                                data={".dockerconfigjson": data}))
        else:
            self._call(self.core_client, "patch_namespaced_secret", name, namespace,
                       V1Secret(metadata=V1ObjectMeta(name=name, namespace=namespace),
                                kind="Secret", type="kubernetes.io/dockerconfigjson",
                                data={".dockerconfigjson": data}))

    def assure_namespace_exist(self, namespace):
        try:
            self._call(self.core_client, "read_namespace", namespace)
        except KubernetesResNotFound:
            self._call(self.core_client, "create_namespace",
                       V1Namespace(metadata=V1ObjectMeta(name=namespace), kind="Namespace"))

    def _encode_secret_data(self, data):
        return {
            k: base64.b64encode(v)
            for k, v in data.items()
        }

    def _decode_secret_data(self, data):
        return {
            k: base64.b64decode(v)
            for k, v in data.items()
        }

    def _call(self, client, func_name, *args, **kwargs):
        kwargs["_request_timeout"] = TIMEOUT
        func = getattr(client, func_name)
        try:
            return func(*args, **kwargs)
        except ApiException as err:
            if err.status == 401:
                raise KubernetesUnauthorized()
            elif err.status == 403:
                raise KubernetesForbidden()
            elif err.status == 404:
                raise KubernetesResNotFound()
            raise KubernetesApiError("{}: {}".format(err.reason, err.body))
        except MaxRetryError:
            msg = "can not connect to kubernetes api server."
            raise KubernetesApiError(msg)
