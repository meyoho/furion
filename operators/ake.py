import datetime
import random
import base64

from django.conf import settings
from app_v2.region.entities import AkeScript

class AkeJobTemplate(object):
    @classmethod
    def get_ake_job_name(cls):
        name = "add-nodes-"
        nowtime = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        randomnum = random.randint(0, 100)
        if randomnum <= 10:
            randomnum = str(0) + str(randomnum)
        job_name = name + str(nowtime) + str(randomnum)
        return job_name

    @classmethod
    def get_ake_job_args(cls, endpoint, node_data):
        job_args = []
        addnodes = "addnodes"
        nodes = "--nodes=" + ';'.join(node_data["node_list"])
        port = "--ssh-port=" + str(node_data["ssh_port"])
        username = "--ssh-username=" + node_data["ssh_username"]
        password = "--ssh-password=" + node_data["ssh_password"]
        secret = AkeSecretTemplate.get_ake_secret_token()
        token = "--token=" + secret["token_id"] + "." + secret["token_secret"]
        apiserver = "--apiserver=" + endpoint.strip("https://")
        debug = "--debug"
        registry = "--registry=$(REGISTRY)"
        pkg_repo = "--pkg-repo=$(PKG)"
        dockercfg = "--dockercfg=/etc/docker/daemon.json"
        for arg in [addnodes, nodes, port, username, password, token, apiserver, registry, pkg_repo, debug, dockercfg]:
            job_args.append(arg)
        return job_args, secret
    
    @classmethod
    def get_ake_job_image(cls, registry, version):
        def _parse_version():
            v = version.split(".")
            return "{}.{}".format(v[0], v[1])
        ake_job_image = "{}:{}".format(registry, _parse_version())
        return ake_job_image


class AkeSecretTemplate(object):
    @classmethod
    def get_ake_secret_token(cls):
        secret = {}
        string_range = "1234567890abcdefghijklmnopqystuvwxyz"
        rand_str = lambda n: ''.join([random.choice(string_range) for i in xrange(n)]) # flake8: noqa
        secret["token_id"] = rand_str(6)
        secret["token_secret"] = rand_str(16)
        return secret
    
    @classmethod
    def get_ake_secret_token_time(cls):
        now = datetime.datetime.now() + datetime.timedelta(days=1)
        return now.isoformat().split('.')[0:-1][0] + "Z"


def generate_node_info(nodes):
    result = []
    for node in nodes:
        cmd = ""
        if node.get('name'):
            cmd += "user={},".format(node['name'])
        if node.get('secret'):
            cmd += "ssh_pass={},".format(node['secret'])
        if node.get('port'):
            cmd += "port={},".format(str(node['port']))
        cmd = cmd.strip(",")
        if cmd:
            result.append("{}({})".format(node['ipaddress'], cmd))
        else:
            result.append(node['ipaddress'])
    return result

def get_master_cidr(masters):
    cidrs = []
    for v in masters:
        ip = v['ipaddress']
        addrs = ip.split(".")
        # sed can't replace / so transfer it
        cidr = "{}.{}.0.0\/16".format(addrs[0], addrs[1])
        if cidr not in cidrs:
            cidrs.append(cidr)
    return cidrs


def append_keys(args, k, v):
    if k not in args.keys():
        args[k] = v


def format(args):
    items = []
    for k, v in args.items():
        items.append("{}={}".format(k, v))
    return " ".join(items)

def generate_ake_script(ake_cluster, token):
    commands = {}
    # chart repo
    chart_repo = settings.AKE_CHART_REPO_URL
    # pkg repo
    pkg_repo = settings.AKE_PKG_REPO_URL

    ake_args = {}
    append_keys(ake_args, '--download-url', settings.AKE_CLUSTER_SETUP_URL.strip("/"))
    if chart_repo:
        append_keys(ake_args, '--chart-repo', chart_repo)
    if pkg_repo:
        append_keys(ake_args, '--pkg-repo', pkg_repo)
    append_keys(ake_args, "--masters", '"{}"'.format(
        ';'.join(generate_node_info(ake_cluster['masters']))))
    if ake_cluster["nodes"]:
        append_keys(ake_args, "--nodes", '"{}"'.format(
            ';'.join(generate_node_info(ake_cluster['nodes']))))
    append_keys(ake_args, "--etcds", '"{}"'.format(
        ';'.join(generate_node_info(ake_cluster['masters']))))
    append_keys(ake_args, "--ssh-username", ake_cluster["ssh"]["name"])
    append_keys(ake_args, '--ssh-port', ake_cluster["ssh"]["port"])
    append_keys(ake_args, "--ssh-type", ake_cluster["ssh"]["type"])
    # secret
    if ake_cluster["ssh"]['type'] == "password":
        append_keys(ake_args, "--ssh-password", '"{}"'.format(ake_cluster["ssh"]["secret"]))
    elif ake_cluster["ssh"]['type'] == "secret":
        append_keys(ake_args, "--ssh-secret", ake_cluster["ssh"]["secret"])
    # network
    append_keys(ake_args, "--network", ake_cluster["cni"]["type"])
    append_keys(ake_args, "--network-opt-cidr", ake_cluster["cni"]["cidr"])
    if ake_cluster["cni"]["type"] == "flannel":
        append_keys(ake_args, "--network-opt-cidr", ake_cluster["cni"]["cidr"])
        append_keys(ake_args, "--network-opt-backend", ake_cluster["cni"]["backend"])
        if ake_cluster["cni"]["network_policy"]:
            append_keys(ake_args, "--network-opt-network-policy", "calico")
    elif ake_cluster["cni"]["type"] == "calico":
        append_keys(ake_args, "--network-opt-backend", 'bird')
    elif ake_cluster["cni"]["type"] == "macvlan":
        append_keys(ake_args, "--monkey-gateway",  ake_cluster["cni"]["macvlan_gateway"])
        append_keys(ake_args, "--monkey-subnet-name",  ake_cluster["cni"]["macvlan_name"])
        append_keys(ake_args, "--monkey-subnet-start",  ake_cluster["cni"]["macvlan_subnet_start"])
        append_keys(ake_args, "--monkey-subnet-end",  ake_cluster["cni"]["macvlan_subnet_end"])
    # ha
    if ake_cluster["cluster_type"]["is_ha"]:
        append_keys(ake_args, "--kube-controlplane-endpoint",
                    ake_cluster["cluster_type"]["loadbalancer"])

    if ake_cluster["apiserver"]:
        append_keys(ake_args, "--apiserver", ake_cluster["apiserver"])
    # image index
    append_keys(ake_args, "--index", settings.REGISTRY_ENDPOINT)
    append_keys(ake_args, "--insecure-registry-list", settings.REGISTRY_ENDPOINT)

    # remote oidc server support
    if settings.SUPPORT_MODE == "STANDARD":
        append_keys(ake_args, "--support-mode", "STANDARD")
        append_keys(ake_args, "--api-gateway", settings.API_GATEWAY.strip('/'))

    # register region data
    append_keys(ake_args, "--token", token)
    append_keys(ake_args, "--register-api", settings.REGISTER_API_ENDPOINT)
    append_keys(ake_args, "--namespace", 'default')
    append_keys(ake_args, "--region-name", ake_cluster['name'])
    append_keys(ake_args, "--region-display-name",
                base64.b64encode(ake_cluster['display_name'].encode('utf-8')))

    # set global resource namespace
    append_keys(ake_args, "--global-namespace", settings.GLOBAL_RESOURCE_NAMESPACE)
    install_command = "curl -kfsSL {}/setup > /tmp/dd.sh ;sudo bash /tmp/dd.sh {}".format(
            settings.AKE_CLUSTER_SETUP_URL.strip("/"), format(ake_args))
    commands["install"] = install_command
    commands["uninstall"] = "remove ake cluster"
    return AkeScript({"commands": commands})
