from __future__ import absolute_import

from django.conf import settings

from mekansm.request import MekRequest
from mekansm import status
from mekansm.exceptions import MekServiceException
from app_v2.region.entities import ApplicationInfo
import logging


LOG = logging.getLogger(__name__)


class FurionRequest(MekRequest):
    @classmethod
    def _parse_response(cls, response, target_source):
        code = response.status_code
        LOG.debug('response status_code={}, content={}'.format(code, (response.content)))

        result = {
            'status_code': code,
            'data': None,
        }

        if code == status.HTTP_204_NO_CONTENT:
            return result
        elif code == status.HTTP_404_NOT_FOUND:
            return {}
        elif code == status.HTTP_503_SERVICE_UNAVAILABLE:
            raise MekServiceException(status.HTTP_503_SERVICE_UNAVAILABLE,
                                      'service is unavailable.', target_source)

        try:
            result['data'] = response.json()
        except Exception as ex:
            LOG.error('Failed to parse response: {} - {}.\n'
                      'Raw response text is {}.'.format(type(ex), ex.message, response.text))
            raise MekServiceException(status.HTTP_500_INTERNAL_SERVER_ERROR,
                                      'service data was not in valid json format.', target_source)

        if not status.is_success(code):
            raise MekServiceException(code, response.text, target_source)
        return result


class ApplicationNotFound(Exception):

    def __init__(self, id):
        self.id = id

    def __str__(self):
        return "application {} not found".format(self.id)


class ApplicationOperator:

    @staticmethod
    def get_template(*args, **kwargs):
        return Chen2Operator.get_template(*args, **kwargs)

    @staticmethod
    def list_public_templates(*args, **kwargs):
        return Chen2Operator.list_public_templates(*args, **kwargs)

    @staticmethod
    def create_template_app(*args, **kwargs):
        return Chen2Operator.create_template_app(*args, **kwargs)

    @staticmethod
    def get_application_instance(*args, **kwargs):
        try:
            return JakiroOperator.get_application_instance(*args, **kwargs)
        except MekServiceException as err:
            if err.status_code == 404:
                raise ApplicationNotFound(id)
            raise err

    @staticmethod
    def get_appcore_instance(*args, **kwargs):
        try:
            return JakiroOperator.get_appcore_instance(*args, **kwargs)
        except MekServiceException as err:
            if err.status_code == 404:
                return {}
            raise err

    @staticmethod
    def check_application_instance(*args, **kwargs):
        try:
            return JakiroOperator.check_application_instance(*args, **kwargs)
        except MekServiceException as err:
            if err.status_code == 404:
                raise ApplicationNotFound(id)
            raise err

    @staticmethod
    def list_application_instances(*args, **kwargs):
        return JakiroOperator.list_application_instances(*args, **kwargs)

    @staticmethod
    def list_appcore_instances(*args, **kwargs):
        return JakiroOperator.list_appcore_instances(*args, **kwargs)

    @staticmethod
    def delete_application_instance(*args, **kwargs):
        return JakiroOperator.delete_application_instance(*args, **kwargs)

    @staticmethod
    def delete_appcore_instance(*args, **kwargs):
        return JakiroOperator.delete_appcore_instance(*args, **kwargs)


class Chen2Operator(MekRequest):
    endpoint = settings.CHEN2_ENDPOINT

    @classmethod
    def get_template(cls, template_id):
        return cls.send("/v1/catalog/templates/{}".format(template_id), method="GET")["data"]

    @classmethod
    def list_public_templates(cls, template_type=None):
        params = {
            "name": "alauda_public_rp"
        }
        if template_type:
            params["template_type"] = template_type
        res = cls.send("/v1/catalog/template_public_repositories", method="GET", params=params)["data"]
        if len(res) <= 0:
            return {}
        return {
            t["name"]: t
            for t in res[0]["templates"] if t['is_active']
        }

    @classmethod
    def create_template_app(cls, region_id, region_name, namespace, name, template, values_yaml_content, token):
        post_data = {
            "name": name,
            "values_yaml_content": values_yaml_content,
            "template": {
                "uuid": template["uuid"],
                "name": template["name"],
                "version": {
                    "uuid": template["versions"][0]["uuid"]
                }
            },
            "namespace": {"name": namespace},
            "cluster": {"uuid": region_id, "name": region_name},
            "user_token": token
        }
        res = cls.send("/v1/catalog/applications", method="POST", data=post_data)["data"]
        # Support old app api
        if res.get("resource", {}).get("uuid", ""):
            return ApplicationInfo(data={
                "uuid": res["resource"]["uuid"],
                "name": res["resource"]["name"],
                "status": res["resource"].get("status", "")
            })
        return ApplicationInfo(data={
            "namespace": res["kubernetes"]["metadata"]["namespace"],
            "name": res["kubernetes"]["metadata"]["name"],
            "status": "Pending"
        })


class KrobelusOperator(MekRequest):
    endpoint = settings.KROBELUS_ENDPOINT


class JakiroOperator(MekRequest):
    endpoint = settings.JAKIRO_ENDPOINT

    @classmethod
    def delete_application_instance(cls, uuid, token):
        headers = {
            "Authorization": "Token {}".format(token)
        }
        cls.send("/v2/apps/{}".format(uuid), method="DELETE", headers=headers)

    @classmethod
    def delete_appcore_instance(cls, region_name, namespace, app_name, token):
        headers = {
            "Authorization": "Token {}".format(token)
        }
        cls.send("/v2/kubernetes/clusters/{}/applications/{}/{}".format(region_name, namespace, app_name),
                 method="DELETE", headers=headers)

    @classmethod
    def get_application_instance(cls, uuid, token):
        headers = {
            "Authorization": "Token {}".format(token)
        }
        data = cls.send("/v2/apps/{}".format(uuid), method="GET", headers=headers)["data"]
        return ApplicationInfo(data={
            "uuid": data["resource"]["uuid"],
            "name": data["resource"]["name"],
            "status": data["resource"]["status"],
            "version": data["catalog_info"]["template"]["version"]["uuid"]
        })

    @classmethod
    def get_appcore_instance(cls, region_name, app_name, namespace, token):
        headers = {
            "Authorization": "Token {}".format(token)
        }
        url = "/v2/kubernetes/clusters/{}/applications/{}/{}/status".format(region_name, namespace, app_name)
        LOG.error("send jakiro: {}   header: {}".format(url, headers))
        data = cls.send(url, method="GET", headers=headers)["data"]
        return ApplicationInfo(data={
            "name": app_name,
            "namespace": namespace,
            "status": data["app_status"]
        })

    @classmethod
    def check_application_instance(cls, uuid, token):
        headers = {
            "Authorization": "Token {}".format(token)
        }
        cls.send("/v2/apps/{}".format(uuid), method="GET", headers=headers)["data"]

    @classmethod
    def list_application_instances(cls, uuids, token):
        headers = {
            "Authorization": "Token {}".format(token)
        }
        apps = []
        # Since jakiro not support /v2/apps with uuid. We use for loop.
        for uuid in uuids:
            data = cls.send("/v2/apps/{}".format(uuid), method="GET", headers=headers)["data"]
            apps.append(ApplicationInfo(data={
                "uuid": data["resource"]["uuid"],
                "name": data["resource"]["name"],
                "status": data["resource"]["status"],
                "version": data["catalog_info"]["template"]["version"]["uuid"]
            }))
        return apps

    @classmethod
    def list_appcore_instances(cls, region_name, namespace, names, token):
        headers = {
            "Authorization": "Token {}".format(token)
        }
        apps = []
        # Since jakiro not support /v2/apps with uuid. We use for loop.
        for name in names:
            data = cls.send("/v2/kubernetes/clusters/{}/applications/{}/{}/status".format(
                    region_name, namespace, name), method="GET", headers=headers)["data"]
            apps.append(ApplicationInfo(data={
                "name": name,
                "namespace": namespace,
                "status": data["app_status"]
            }))
        return apps
