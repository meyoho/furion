from __future__ import absolute_import

from django.conf import settings

from mekansm.request import MekRequest
from mekansm.exceptions import MekServiceException

from app_v2.region.entities import IntegrationInfo


class IntegrationNotFound(Exception):

    def __init__(self, id):
        self.id = id

    def __str__(self):
        return "integration {} not found".format(self.id)


class IntegrationOperator(MekRequest):
    endpoint = settings.DAVION_ENDPOINT

    @classmethod
    def get_integration_instance(cls, id):
        try:
            data = cls.send("/v1/integrations/{}".format(id), method="GET")["data"]
        except MekServiceException as err:
            if err.status_code == 404:
                raise IntegrationNotFound(id)
            raise err
        data["uuid"] = data.pop("id")
        return IntegrationInfo(data=data)

    @classmethod
    def list_integration_instances(cls, uuids):
        params = {"uuids": ",".join(uuids)}
        data = cls.send("/v1/integrations/", method="GET", params=params)["data"]
        for d in data:
            d["uuid"] = d.pop("id")
        return [IntegrationInfo(data=d) for d in data]
