import hashlib

from semantic_version import Version


def md5sum(content):
    m = hashlib.md5()
    m.update(content)
    return m.hexdigest()


def check_versions_matched(ver1, ver2):
    try:
        if Version(ver1, partial=True) == Version(ver2, partial=True):
            return True
        else:
            return False 
    except (ValueError, TypeError):
        return False


def merge_dicts(*dict_args):
    """
    Given any number of dict, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result
