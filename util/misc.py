#!/usr/bin/env python
# -*- coding: utf-8 -*-
from time import time
from alauda.common import collection
from django.conf import settings

from mekansm.request import MekRequest
import logging

from app.models import Region

__author__ = 'Hang Yan'

LOG = logging.getLogger(__name__)


class TinyRequest(MekRequest):
    endpoint = '{}/v2'.format(settings.TINY_ENDPOINT)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': settings.REQUEST_USER_AGENT
    }


def send_metrics(data):
    LOG.debug("Preparing send metrics data to tiny. | num={}".format(len(data)))
    for chunk in collection.chunks(data, 20):
        TinyRequest.send('/metrics-data/', method='POST', data=chunk)


def _check_db():
    Region.objects.first()


def self_diagnose():
    """This function is used to check whether furion is running ok."""

    def _process_one(name):
        data = {
            "status": "OK",
            "name": name,
            "message": "",
            "latency": ""
        }
        start_time = time()
        try:
            globals()['_check_{}'.format(name)]()
            data['latency'] = "{}s".format(time() - start_time)
            return data
        except Exception as e:
            LOG.error("Diagnose check error: {}".format(e))
            data['message'] = e.message
            data['latency'] = "{}ms".format(time() - start_time)
        data['status'] = "ERROR"
        return data

    def _cal_global_status(dt):
        assert isinstance(dt, list)
        for x in dt:
            assert isinstance(x, dict)
            if x.get('status') == 'ERROR':
                return 'ERROR'
        return 'OK'

    details = []
    for name in ['db']:
        details.append(_process_one(name))
    return {
        "details": details,
        "status": _cal_global_status(details)
    }
