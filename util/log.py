#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time

import logging

from django.conf import settings

__author__ = 'Hang Yan'

LOG = logging.getLogger(__name__)


def _log_request(request, attrs):
    method = request.method
    if method not in ['PUT', 'POST']:
        return
    path = request.get_full_path()
    res = '\n'.join(['{} = {}'.format(x, getattr(request, x, '')) for x in attrs])
    LOG.debug("{} {} | {}".format(method, path, res))


class LoggingMiddleware(object):
    def process_request(self, request):
        self.start_time = time.time()
        _log_request(request, ['body'])

    def process_response(self, request, response):
        try:
            path = request.get_full_path().split('/')[-1:][0]
            if path in ['_ping', '_diagnose', 'ping']:
                return response
            remote_addr = request.META.get('REMOTE_ADDR')
            if remote_addr in getattr(settings, 'INTERNAL_IPS', []):
                remote_addr = request.META.get('HTTP_X_FORWARDED_FOR') or remote_addr
            extra_log = ""
            req_time = time.time() - self.start_time
            content_len = len(response.content)
            # sql_time = sum(float(q['time']) for q in connection.queries) * 1000
            # extra_log += " (%s SQL queries, %s ms)" % (len(connection.queries), sql_time)
            LOG.debug("%s  %s %s %s %s (%.02f seconds)%s",
                      remote_addr, request.method,
                      request.get_full_path(), response.status_code,
                      content_len, req_time, extra_log)
        except Exception as e:
            LOG.warning("LoggingMiddleware Error: %s" % e)
        return response
