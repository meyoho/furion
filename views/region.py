from __future__ import absolute_import

from alauda.clients.furion import FurionClient
from rest_framework.views import APIView
from controls.region import RegionControl
from util import const
from alauda.common.web import json_response
from alauda.tracing import tracing_response_time

__author__ = 'Hang Yan'


class RegionsView(APIView):
    @tracing_response_time(module='regions')
    def get(self, request):
        regions = RegionControl.list(**request.query_params)
        result = []
        for x in regions:
            if x['platform_version'] != const.KUBERNETES_PLATFORM_VERSION_V4:
                result.append(FurionClient(region=x).merge_region())
                continue
            result.append(x)
        return json_response(result)


class RegionView(APIView):

    @tracing_response_time(module='region')
    def get(self, request, region_id):
        data = RegionControl.get_info(id=region_id)
        merged = data
        if data['platform_version'] != const.KUBERNETES_PLATFORM_VERSION_V4:
            merged = FurionClient(region=data).merge_region()
        return json_response(merged)
