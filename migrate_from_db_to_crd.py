# flake8: noqa
import sys
import os
import django
sys.path.append('/furion')
os.environ['DJANGO_SETTINGS_MODULE'] = 'furion.settings'
django.setup()
from app.models import Region
import copy
import base64
from django.conf import settings
from app_v2.region.entities import CLUSTER_REGISTRY_TEMPLATE, CLUSTER_SECRET_TEMPLATE, RegionGetData
from operators.kubernetes import KubernetesManager
from app_v2.region.core import format_cluster_input


ENDPOINT = settings.KUBERNETES_ENDPOINT


def get_regions():
    return Region.objects.filter(platform_version="v4")


def sync_regions(token):
    k8s_operator = KubernetesManager(endpoint=ENDPOINT,
                                     token=token)
    regions = get_regions()
    print "Start migrate {} regions".format(len(regions))
    for r in regions:
        region = RegionGetData(r)
        print "migrating region: {}".format(region.data['name'])
        cluster = copy.deepcopy(CLUSTER_REGISTRY_TEMPLATE)
        secret = copy.deepcopy(CLUSTER_SECRET_TEMPLATE)
        secret['data']['token'] = base64.b64encode(region.data["attr"]["kubernetes"]["token"])
        secret['metadata']['name'] = "{}{}".format(settings.GLOBAL_RESOURCE_PREFIX,
                                                   region.data['name'])
        k8s_operator.create_cluster_secret(secret, secret['metadata']['namespace'])
        cluster['metadata']['name'] = region.data['name']
        cluster['spec']['authInfo']["controller"]['name'] = secret['metadata']['name']
        cluster['spec']['kubernetesApiEndpoints']['serverEndpoints'].append({
            "clientCIDR": region.data['attr']['kubernetes'].get("cni", {}).get("cidr", ""),
            "serverAddress": region.data['attr']['kubernetes']["endpoint"]
        })

        format_cluster_input(cluster, region)
        print "create cluster: {}".format(cluster)
        k8s_operator.create_cluster(cluster, cluster['metadata']['namespace'])


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "USAGE: python migrate_from_db_to_crd.py [token]"
        os.exit(1)
    token = sys.argv[1]
    sync_regions(token)
