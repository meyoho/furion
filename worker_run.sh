#!/usr/bin/env bash

cd "$(dirname "$0")"

cp /furion/conf/supervisord_worker.conf  /furion/conf/supervisord.conf

/usr/bin/supervisord --nodaemon
