from django.conf.urls import url

from .region import api as region_api
from .mirror import api as mirror_api


urlpatterns = [
    url(r'^regions', [region_api, None, None]),
    url(r'^mirrors', [mirror_api, None, None]),
]
