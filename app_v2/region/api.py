from django.utils.decorators import method_decorator
from django.conf.urls import url
from django.http import Http404
from django.conf import settings
from mekansm.exceptions import MekAPIException
from mekansm.errors import Types as APIErrorType
from rest_framework import status, viewsets
from rest_framework.response import Response

from gateway.database import RegionDBGateway
from gateway.cache import CacheGateway

from .entities import (RegionCheckData, RegionData, AddNodesData,
                       FEATURES_MAP, LabelNodeData, RegionUpdateData)
from . import core

UUID_PATTERN = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"
NAME_PATTERN = "[A-Za-z0-9-_.]+"


class RegionError(MekAPIException):

    def __init__(self, code, error_type, message):
        self.code = code
        self._error_type = error_type
        self._message = message

    @property
    def error_type(self):
        return self._error_type

    @property
    def message(self):
        return self._message


def handle_errors(errors):
    def error_decorator(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as exc:
                for err in errors:
                    if exc.__class__.__name__ != err["name"]:
                        continue
                    else:
                        code = err.get("code", err["name"])
                        error_type = err.get("error_type", APIErrorType.SERVER_ERROR)
                        message = err.get("message", str(exc))
                        raise RegionError(code, error_type, message)
                raise exc
        return wrapper
    return error_decorator


class RegionCommonViewSet(viewsets.ViewSet):

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "VersionNotSupported", "error_type": APIErrorType.BAD_REQUEST}
    ]))
    def check_region(self, request):
        check_data = RegionCheckData(data=request.data)
        version = core.check_region(CacheGateway, check_data)
        return Response({"version": version})

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "VersionNotSupported", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesResNotFound", "error_type": APIErrorType.NOT_FOUND}
    ]))
    def label_node(self, request, region_id, node_name):
        labels_data = LabelNodeData(data=request.data)
        core.label_node(RegionDBGateway, CacheGateway, request, region_id, node_name, labels_data)
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "VersionNotSupported", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesResNotFound", "error_type": APIErrorType.NOT_FOUND}
    ]))
    def delete_node(self, request, region_id, node_name):
        core.delete_node(RegionDBGateway, CacheGateway, request, region_id, node_name)
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "VersionNotSupported", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesResNotFound", "error_type": APIErrorType.NOT_FOUND}
    ]))
    def cordon_node(self, request, region_id, node_name):
        core.cordon_node(RegionDBGateway, CacheGateway, request, region_id, node_name, True)
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "VersionNotSupported", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesResNotFound", "error_type": APIErrorType.NOT_FOUND}
    ]))
    def uncordon_node(self, request, region_id, node_name):
        core.cordon_node(RegionDBGateway, CacheGateway, request, region_id, node_name, False)
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
    ]))
    def list_nodes(self, request, region_id):
        node_list = core.list_nodes(RegionDBGateway, CacheGateway, request, region_id)
        return Response(node_list.data)

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
    ]))
    def add_nodes(self, request, region_id):
        addnodes_data = AddNodesData(data=request.data)
        core.add_nodes(RegionDBGateway, CacheGateway, request, region_id, addnodes_data)
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
    ]))
    def list_nodes_labels(self, request, region_id):
        nodes_label = core.list_nodes_labels(RegionDBGateway, CacheGateway, request, region_id)
        return Response(nodes_label)

    def generate_installation_command(self, request):
        commands = core.generate_installation_command(request)
        return Response(commands.data, status=status.HTTP_200_OK)


class RegionCRDViewSet(viewsets.ViewSet):

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "VersionNotSupported", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "RegionRegistered", "error_type": APIErrorType.BAD_REQUEST}
    ]))
    def create_region(self, request):
        region = RegionData(data=request.data)
        region = core.create_region_crd(request, CacheGateway, region)
        return Response(region.data,
                        status=status.HTTP_201_CREATED)

    def list_regions(self, request):
        region_ids = request.query_params.get("uuids", "").split(",")
        regions = core.list_regions_crd(request, CacheGateway, region_ids)
        return Response([r.data for r in regions])

    @method_decorator(handle_errors([
        {"name": "DoesNotExist", "error_type": APIErrorType.NOT_FOUND},
        {"name": "KubernetesResNotFound", "error_type": APIErrorType.NOT_FOUND},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST}
    ]))
    def get_region(self, request, region_id):
        region = core.get_region_crd(request, CacheGateway, region_id)
        return Response(region.data)

    @method_decorator(handle_errors([
        {"name": "KubernetesResNotFound", "error_type": APIErrorType.NOT_FOUND},
        {"name": "DoesNotExist", "error_type": APIErrorType.NOT_FOUND},
    ]))
    def delete_region(self, request, region_id):
        force_delete = request.query_params.get("force")
        core.delete_region_crd(request, CacheGateway, region_id, force_delete)
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    @method_decorator(handle_errors([
        {"name": "KubernetesResNotFound", "error_type": APIErrorType.NOT_FOUND},
        {"name": "DoesNotExist", "error_type": APIErrorType.NOT_FOUND},
    ]))
    def update_region(self, request, region_id):
        validated_data = RegionUpdateData(data=request.data)
        validated_data.is_valid(raise_exception=True)
        update_type = request.query_params.get("type", core.UPDATE_PATCH)
        core.update_region(RegionDBGateway, CacheGateway, region_id, validated_data, update_type)
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    @method_decorator(handle_errors([
        {"name": "FunctionNotSupported", "error_type": APIErrorType.BAD_REQUEST},
    ]))
    def list_features(self, request, region_id):
        raise core.FunctionNotSupported("feature")

    @method_decorator(handle_errors([
        {"name": "FunctionNotSupported", "error_type": APIErrorType.BAD_REQUEST},
    ]))
    def add_feature(self, request, region_id, feature_name):
        raise core.FunctionNotSupported("feature")

    @method_decorator(handle_errors([
        {"name": "FunctionNotSupported", "error_type": APIErrorType.BAD_REQUEST},
    ]))
    def get_feature(self, request, region_id, feature_name):
        raise core.FunctionNotSupported("feature")

    @method_decorator(handle_errors([
        {"name": "FunctionNotSupported", "error_type": APIErrorType.BAD_REQUEST},
    ]))
    def update_feature(self, request, region_id, feature_name):
        raise core.FunctionNotSupported("feature")

    @method_decorator(handle_errors([
        {"name": "FunctionNotSupported", "error_type": APIErrorType.BAD_REQUEST},
    ]))
    def delete_feature(self, request, region_id, feature_name):
        raise core.FunctionNotSupported("feature")


class RegionDBViewSet(viewsets.ViewSet):

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "VersionNotSupported", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "RegionRegistered", "error_type": APIErrorType.BAD_REQUEST}
    ]))
    def create_region(self, request):
        region = RegionData(data=request.data)
        region = core.create_region_db(RegionDBGateway, CacheGateway, region)
        return Response(region.data,
                        status=status.HTTP_201_CREATED)

    def list_regions(self, request):
        region_ids = request.query_params.get("uuids", "").split(",")
        regions = core.list_regions_db(RegionDBGateway, CacheGateway, region_ids)
        return Response([r.data for r in regions])

    @method_decorator(handle_errors([
        {"name": "DoesNotExist", "error_type": APIErrorType.NOT_FOUND},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST}
    ]))
    def get_region(self, request, region_id):
        region = core.get_region_db(RegionDBGateway, CacheGateway, region_id)
        return Response(region.data)

    @method_decorator(handle_errors([
        {"name": "DoesNotExist", "error_type": APIErrorType.NOT_FOUND},
    ]))
    def delete_region(self, request, region_id):
        force_delete = request.query_params.get("force")
        core.delete_region_db(RegionDBGateway, CacheGateway, region_id, force_delete)
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    @method_decorator(handle_errors([
        {"name": "DoesNotExist", "error_type": APIErrorType.NOT_FOUND},
    ]))
    def update_region(self, request, region_id):
        validated_data = RegionUpdateData(data=request.data)
        validated_data.is_valid(raise_exception=True)
        update_type = request.query_params.get("type", core.UPDATE_PATCH)
        core.update_region(RegionDBGateway, CacheGateway, region_id, validated_data, update_type)
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    def list_features(self, request, region_id):
        token = request.query_params.get("token", "")
        features = core.list_features(RegionDBGateway, CacheGateway, region_id, token)
        return Response(features)

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "FeatureNotSupported", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "FeatureNotSupportIntegration", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "FeatureAlreadyAdded", "error_type": APIErrorType.BAD_REQUEST},
    ]))
    def add_feature(self, request, region_id, feature_name):
        if feature_name not in FEATURES_MAP:
            raise Http404
        feature_create_data = FEATURES_MAP[feature_name]["feature_create_cls"](data=request.data)
        feature = core.add_feature(RegionDBGateway, CacheGateway, region_id, feature_name, feature_create_data)
        return Response(feature.data)

    def get_feature(self, request, region_id, feature_name):
        if feature_name not in FEATURES_MAP:
            raise Http404
        token = request.query_params.get("token", "")
        feature = core.get_feature(RegionDBGateway, CacheGateway, region_id, feature_name, token)
        return Response(feature)

    def update_feature(self, request, region_id, feature_name):
        if feature_name not in FEATURES_MAP:
            raise Http404
        feature_create_data = FEATURES_MAP[feature_name]["feature_create_cls"](data=request.data)
        feature = core.update_feature(RegionDBGateway, CacheGateway, region_id,
                                      feature_name, feature_create_data)
        return Response(feature.data)

    def delete_feature(self, request, region_id, feature_name):
        if feature_name not in FEATURES_MAP:
            raise Http404
        token = request.query_params.get("token", "")
        core.delete_feature(RegionDBGateway, CacheGateway, token, region_id, feature_name)
        return Response(None, status=status.HTTP_204_NO_CONTENT)


RegionViewSet = RegionCRDViewSet
if settings.STORAGE_BACKEND == "DB":
    RegionViewSet = RegionDBViewSet


urlpatterns = [
    url(r"^/generate-installation-command/?$",
        RegionCommonViewSet.as_view({
            "post": "generate_installation_command"
        })),
    url(r"^/version-check/?$",
        RegionCommonViewSet.as_view({
            "post": "check_region"
        })),
    url(r"^/?$",
        RegionViewSet.as_view({
            "post": "create_region",
            "get": "list_regions"
        })),
    url(r"^/(?P<region_id>{})/?$".format(NAME_PATTERN),
        RegionViewSet.as_view({
            "get": "get_region",
            "put": "update_region",
            "delete": "delete_region"
        })),
    url(r"^/(?P<region_id>{})/nodes/?$".format(NAME_PATTERN),
        RegionCommonViewSet.as_view({
            "get": "list_nodes",
            "post": "add_nodes"
        })),
    url(r"^/(?P<region_id>{})/nodes/(?P<node_name>{})?$".format(NAME_PATTERN, NAME_PATTERN),
        RegionCommonViewSet.as_view({
            "delete": "delete_node"
        })),
    url(r"^/(?P<region_id>{})/nodes/(?P<node_name>{})/labels?$".format(NAME_PATTERN, NAME_PATTERN),
        RegionCommonViewSet.as_view({
            "put": "label_node"
        })),
    url(r"^/(?P<region_id>{})/nodes/(?P<node_name>{})/cordon?$".format(NAME_PATTERN, NAME_PATTERN),
        RegionCommonViewSet.as_view({
            "put": "cordon_node"
        })),
    url(r"^/(?P<region_id>{})/nodes/(?P<node_name>{})/uncordon?$".format(NAME_PATTERN, NAME_PATTERN),
        RegionCommonViewSet.as_view({
            "put": "uncordon_node"
        })),
    url(r"^/(?P<region_id>{})/labels/?$".format(NAME_PATTERN),
        RegionCommonViewSet.as_view({
            "get": "list_nodes_labels"
        })),
    url(r"^/(?P<region_id>{})/features/?$".format(NAME_PATTERN),
        RegionViewSet.as_view({
            "get": "list_features"
        })),
    url(r"^/(?P<region_id>{})/features/(?P<feature_name>{})?$".format(NAME_PATTERN, NAME_PATTERN),
        RegionViewSet.as_view({
            "post": "add_feature",
            "get": "get_feature",
            "put": "update_feature",
            "delete": "delete_feature"
        })),
]
