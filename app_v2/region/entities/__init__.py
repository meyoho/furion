from .data_type import DataType
from .region_attrs import *  # flake8:noqa
from .region_features import *  # flake8:noqa
from util.const import KUBERNETES_PLATFORM_VERSION_V4, CONTAINER_MANAGER_K8S
from django.conf import settings
from app.models import Region
from rest_framework import serializers
import re

IP_PATTERN = re.compile(r"^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$")
SSH_TYPES = [("password", "password"), ("secret", "secret")]
CNI_TYPES = [("flannel", "flannel"), ("macvlan", "macvlan"), ("calico", "calico"), ("kube-ovn", "kube-ovn"), ("none", "none")]
FLANNEL_BACKEND = ['host-gw', 'vxlan']
MACVLAN_ARGS = ['macvlan_gateway', 'macvlan_name', 'macvlan_subnet_start', 'macvlan_subnet_end']
MACVLAN_IP_ARGS = ['macvlan_gateway', 'macvlan_subnet_start', 'macvlan_subnet_end']


class RegionAttr(DataType):
    cluster = ClusterInfo()
    cloud = CloudInfo()
    docker = DockerInfo()
    kubernetes = KubernetesInfo()
    feature_namespace = serializers.CharField(required=False)

    def to_representation(self, instance):
        ret = super(RegionAttr, self).to_representation(instance)
        if not ret.get("feature_namespace"):
            ret["feature_namespace"] = settings.FEATURE_REGION_CONF['namespace']
        return ret


class RegionFeature(DataType):
    volume = VolumeFeature(required=False)
    lb = LoadBalancerFeature(required=False)
    registry = RegistryFeature(required=False)
    log = LogFeature(required=False)
    metric = MetricFeature(required=False)
    customized = serializers.DictField(required=False)
    service_catalog = serializers.DictField(required=False)
    dashboard = serializers.DictField(required=False)
    devops = serializers.DictField(required=False)
    ssh = SshFeature(required=False)

    def to_representation(self, instance):
        ret = super(RegionFeature, self).to_representation(instance)
        if ret.get("service_catalog"):
            ret['service-catalog'] = ret.pop('service_catalog')
        return ret


class AutoValue(DataType):
    kind = serializers.ChoiceField(choices=(("configmap", "ConfigMap"), ("secret", "Secret"),
                                            ("docker-config-secret", "Docker Config Secret")))
    name = serializers.CharField()
    value = serializers.CharField()


class IntegrationInfo(DataType):
    uuid = serializers.UUIDField()
    type = serializers.CharField()
    enabled = serializers.BooleanField()


class ApplicationInfo(DataType):
    uuid = serializers.UUIDField(required=False)
    name = serializers.CharField(required=False)
    namespace = serializers.CharField(required=False)
    status = serializers.CharField(allow_blank=True)
    version = serializers.UUIDField(required=False)


class FeatureCreateData(DataType):
    auto_values = serializers.ListField(child=AutoValue())
    values_yaml_content = serializers.CharField(allow_blank=True, required=False)
    token = serializers.CharField()


class LogFeatureCreateData(FeatureCreateData):
    config = LogFeature()


class MetricFeatureCreateData(FeatureCreateData):
    config = MetricFeature()

    
class SshFeatureCreateData(FeatureCreateData):
    config = SshFeature()


class SshFeatureResp(DataType):
    config = SshFeature()
    application_info = ApplicationInfo()

    
class SshFeatureIntegrationResp(DataType):
    config = SshFeature()
    integration_info = IntegrationInfo()


class LogFeatureResp(DataType):
    config = LogFeature()
    application_info = ApplicationInfo()


class MetricFeatureResp(DataType):
    config = MetricFeature()
    application_info = ApplicationInfo()


class MetricFeatureIntegrationResp(DataType):
    config = MetricFeature()
    integration_info = IntegrationInfo()


class RegistryFeatureCreateData(FeatureCreateData):
    config = RegistryFeature()


class RegistryFeatureResp(DataType):
    config = RegistryFeature()
    application_info = ApplicationInfo()


FEATURES_MAP = {
    "log": {
        "support_integration": False,
        "feature_create_cls": LogFeatureCreateData,
        "official_response_cls": LogFeatureResp
    },
    "metric": {
        "support_integration": True,
        "feature_create_cls": MetricFeatureCreateData,
        "official_response_cls": MetricFeatureResp,
        "integration_response_cls": MetricFeatureIntegrationResp
    },
    "ssh": {
        "support_integration": True,
        "feature_create_cls": SshFeatureCreateData,
        "official_response_cls": SshFeatureResp,
        "integration_response_cls": SshFeatureIntegrationResp
    },
    "registry": {
        "support_integration": False,
        "feature_create_cls": RegistryFeatureCreateData,
        "official_response_cls": RegistryFeatureResp
    }
}


class RegionCheckData(DataType):
    endpoint = serializers.URLField()
    token = serializers.CharField()

    def __init__(self, *args, **kwargs):
        if len(args) > 0 and type(args[0]) == RegionCheckData:
            if 'endpoint' in args[0]:
                args[0]['endpoint'] = args[0]['endpoint'].strip('/')
        data = None
        if 'data' in kwargs:
            data = kwargs['data']
        elif len(args) == 2:
            data = args[1]
        if (data and 'endpoint' in data):
            data['endpoint'] = data['endpoint'].strip('/')
        super(RegionCheckData, self).__init__(*args, **kwargs)


class SimpleRegion(DataType):
    id = serializers.UUIDField()
    name = serializers.CharField()
    display_name = serializers.CharField()
    created_at = serializers.DateTimeField()


class Mirror(DataType):
    id = serializers.UUIDField(read_only=True)
    name = serializers.CharField()
    display_name = serializers.CharField()
    flag = serializers.CharField()

    def to_representation(self, instance):
        ret = super(Mirror, self).to_representation(instance)
        ret["regions"] = [SimpleRegion(r).data for r in instance.region_set.all()]
        return ret


class MirrorGetData(Mirror):
    id = serializers.UUIDField(required=False)
    name = serializers.CharField(required=False)
    display_name = serializers.CharField(required=False)
    flag = serializers.CharField(required=False)
    regions = serializers.ListSerializer(child=SimpleRegion(), required=False)


class RegionData(DataType):
    id = serializers.UUIDField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)
    state = serializers.CharField(read_only=True)
    name = serializers.CharField(max_length=36, allow_blank=True, default="")
    namespace = serializers.CharField(max_length=36, allow_blank=True, default="")
    display_name = serializers.CharField()
    attr = RegionAttr()
    features = RegionFeature(default={})
    container_manager = serializers.CharField(required=False, default=CONTAINER_MANAGER_K8S)
    platform_version = serializers.CharField(required=False,
                                             default=KUBERNETES_PLATFORM_VERSION_V4)
    mirror = Mirror(read_only=True)

    def __init__(self, *args, **kwargs):
        if len(args) > 0 and type(args[0]) == Region:
            if 'service-catalog' in args[0].features:
                args[0].features['service_catalog'] = args[0].features.pop('service-catalog')
            if 'kubernetes' in args[0].attr and 'endpoint' in args[0].attr['kubernetes']:
                args[0].attr['kubernetes']['endpoint'] = args[0].attr.get('kubernetes', {}).pop('endpoint').strip('/')
        data = None
        if 'data' in kwargs:
            data = kwargs['data']
        elif len(args) == 2:
            data = args[1]
        if (data and 'features' in data
            and 'service-catalog' in data['features']):
            data['features']['service_catalog'] = data['features'].pop('service-catalog')
        if (data and 'attr' in data and 'kubernetes' in data['attr']
            and 'endpoint' in data['attr']['kubernetes']):
            data['attr']['kubernetes']['endpoint'] = data['attr']['kubernetes'].pop('endpoint').strip('/')
        super(RegionData, self).__init__(*args, **kwargs)

    def to_representation(self, instance):
        if isinstance(instance, Region):
            ret = super(RegionData, self).to_representation(instance)
            if not ret.get("mirror"):
                ret["mirror"] = {}
            return ret
        return instance

    def validate_container_manager(self, value):
        return "KUBERNETES"

    def validate_platform_version(self, value):
        return "v4"


class RegionGetData(RegionData):
    id = serializers.UUIDField(required=False)
    mirror = MirrorGetData(required=False)
    created_at = serializers.DateTimeField(required=False)
    updated_at = serializers.DateTimeField(required=False)
    state = serializers.CharField(required=False)


class RegionUpdateData(serializers.Serializer):
    display_name = serializers.CharField(required=False)
    attr = RegionAttr(required=False)
    features = RegionFeature(required=False)
    platform_version = serializers.ChoiceField(required=False, choices=(("v4", "v4")))


class NodeListData(DataType):
    items = serializers.ListField(child=serializers.DictField())
    kind = serializers.CharField()
    api_version = serializers.CharField()
    metadata = serializers.DictField()


class AddNodesData(DataType):
    node_list = serializers.ListSerializer(child=serializers.CharField())
    ssh_port = serializers.IntegerField()
    ssh_username = serializers.CharField()
    ssh_password = serializers.CharField()


class LabelNodeData(DataType):
    labels = serializers.DictField(required=True)


CLUSTER_REGISTRY_TEMPLATE = {
    "apiVersion": "clusterregistry.k8s.io/v1alpha1",
    "kind": "Cluster",
    "metadata": {
        "annotations":{},
        "name": "",
        "namespace": settings.GLOBAL_RESOURCE_NAMESPACE
    },
    "spec": {
        "authInfo": {
            "controller": {
                "kind": "Secret",
                "name": "",
                "namespace": settings.GLOBAL_RESOURCE_NAMESPACE
            }
        },
        "kubernetesApiEndpoints": {
            "serverEndpoints": [
            ]
        }
    }
}


CLUSTER_SECRET_TEMPLATE = {
    "apiVersion": "v1",
    "data": {
        "token": ""
    },
    "kind": "Secret",
    "metadata": {
        "name": "",
        "namespace": settings.GLOBAL_RESOURCE_NAMESPACE
    },
    "type": "Opaque"
}

class ClusterType(DataType):
    is_ha = serializers.BooleanField(required=False, default=False)
    loadbalancer = serializers.CharField(required=False, allow_blank=True)

class Ssh(DataType):
    type = serializers.ChoiceField(choices=SSH_TYPES)
    name = serializers.CharField()
    secret = serializers.CharField()
    port = serializers.IntegerField(required=False, default=22)

class Cni(DataType):
    type = serializers.ChoiceField(required=True, choices=CNI_TYPES)
    backend = serializers.CharField(required=False, allow_blank=True)
    cidr = serializers.CharField(required=False, allow_blank=True)
    network_policy = serializers.CharField(required=False, allow_blank=True)
    macvlan_gateway = serializers.CharField(required=False, allow_blank=True)
    macvlan_name = serializers.CharField(required=False, allow_blank=True)
    macvlan_subnet_start = serializers.CharField(required=False, allow_blank=True)
    macvlan_subnet_end = serializers.CharField(required=False, allow_blank=True)

    def validate(self, data):
        if data['type'] is not None and data['cidr'] is None:
            raise serializers.ValidationError( data['type'] + " must specify cidr")
        if data['type'] == 'flannel':
            if data['backend'] not in FLANNEL_BACKEND:
                raise  serializers.ValidationError("flannel backend must in {}".format(FLANNEL_BACKEND))
        if data['type'] == 'macvlan':
            if not (set(MACVLAN_ARGS) < set(data.keys())):
                raise  serializers.ValidationError("macvlan must specify {}".format(MACVLAN_ARGS))
            for ip in MACVLAN_IP_ARGS:
                if not IP_PATTERN.match(data[ip]):
                    raise  serializers.ValidationError("{} is not a valid ip".format(data[ip]))
        return data

class AkeCluster(DataType):
    name = serializers.CharField(max_length=36)
    display_name = serializers.CharField()
    apiserver = serializers.CharField(required=False, allow_blank=True)
    cluster_type = ClusterType()
    masters = serializers.ListField()
    nodes = serializers.ListField()
    ssh = Ssh()
    cni = Cni()

    def validate(self, data):
        for i in data['masters'] + data['nodes']:
            if not IP_PATTERN.match(i['ipaddress']):
                raise  serializers.ValidationError("{} is not a valid ip".format(i['ipaddress']))
        if data['cluster_type']['is_ha'] and len(data['masters']) < 2:
            raise serializers.ValidationError("master node number must greater than 2")
        if data['cluster_type']["is_ha"] and not data['cluster_type']['loadbalancer']:
            raise serializers.ValidationError("must specify loadbalancer in ha mode")
        return data

class AkeCommand(DataType):
    install = serializers.CharField()
    uninstall = serializers.CharField()

class AkeScript(DataType):
    commands = AkeCommand()