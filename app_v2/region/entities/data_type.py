from rest_framework import serializers


class DataType(serializers.Serializer):
    def __init__(self, *args, **kwargs):
        self.need_validate = kwargs.pop("need_validate", True)
        super(DataType, self).__init__(*args, **kwargs)
        if hasattr(self, "initial_data"):
            self.is_valid(raise_exception=True)

    @property
    def data(self):
        if not self.need_validate and hasattr(self, "initial_data"):
            return self.initial_data
        if hasattr(self, "_data"):
            delattr(self, "_data")
        return super(DataType, self).data
