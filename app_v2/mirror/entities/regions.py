from rest_framework import serializers

from .data_type import DataType


class MirrorRegionData(DataType):
    name = serializers.CharField(required=True)
    id = serializers.UUIDField(required=True)
    display_name = serializers.CharField(required=False)
    created_at = serializers.DateTimeField(required=False)
