#!/usr/bin/env python
# -*- coding: utf-8 -*-
# flake8: noqa

import sys
import os

import django
from Crypto.Cipher import AES
from django.utils.importlib import import_module

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'furion.settings')
django.setup()
import_module('furion')

from django.conf import settings
from alauda.common.func import encode_aes, decode_aes, pad_str
from app.models import Region


if __name__ == '__main__':
    print 'migrate begins'
    cipher = AES.new(pad_str(settings.SECRET_KEY)[:32])
    regions = Region.objects.all()
    count = 0
    lost_ones = []
    for region in regions:
        tunnel = region.features.get('tunnel')
        if tunnel:
            private_key = tunnel['private_key']
            public_key = tunnel['public_key']
            if not private_key or not public_key:
                lost_ones.append(region.id)
            if 'reverse' in sys.argv:
                tunnel['private_key'], tunnel['public_key'] = \
                    decode_aes(cipher, private_key), decode_aes(cipher, public_key)
            else:
                tunnel['private_key'], tunnel['public_key'] = \
                    encode_aes(cipher, private_key), encode_aes(cipher, public_key)
            region.save()
            count = count + 1

    print 'total rows: ', count
    if len(lost_ones):
        print 'total lost: ', len(lost_ones)
        print 'lost ones: ', lost_ones
    else:
        print 'migrate success'
