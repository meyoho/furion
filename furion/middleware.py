from django.http.response import HttpResponse

from mekansm.exceptions import MekAPIException
import json
import logging
import traceback

logger = logging.getLogger(__name__)


class NonHtmlDebugToolbarMiddleware(object):
    """
    The Django Debug Toolbar usually only works for views that return HTML.
    This middleware wraps any non-HTML response in HTML if the request
    has a 'debug' query parameter (e.g. http://localhost/foo?debug)
    Special handling for json (pretty printing) and
    binary data (only show data length)
    """

    @staticmethod
    def process_response(request, response):
        if request.GET.get('debug') == '':
            if response['Content-Type'] == 'application/octet-stream':
                new_content = '<html><body>Binary Data, ' \
                              'Length: {}</body></html>'.format(len(response.content))
                response = HttpResponse(new_content)
            elif response['Content-Type'] != 'text/html':
                content = response.content
                try:
                    json_ = json.loads(content)
                    content = json.dumps(json_, sort_keys=True, indent=2)
                except ValueError:
                    pass
                response = HttpResponse('<html><body><pre>{}'
                                        '</pre></body></html>'.format(content))

        return response


class CommonExceptionMiddleware():
    def process_exception(self, request, exception):
        logger.error('Exception was not wrapped: {}'.format(str(exception)))
        logger.error(traceback.format_exc())
        exception = MekAPIException('unknown_issue', message=str(exception))
        error_response = {'errors': [exception.data]}
        return HttpResponse(json.dumps(error_response), content_type='application/json',
                            status=exception.status_code)

    def process_request(self, request):
        pass
