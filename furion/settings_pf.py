from .settings import *  # NOQA

DEBUG = True

MIDDLEWARE_CLASSES += (  # NOQA
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'furion.middleware.NonHtmlDebugToolbarMiddleware',
    'silk.middleware.SilkyMiddleware',
)

INSTALLED_APPS += (  # NOQA
    'debug_toolbar',
    'silk',
)

DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'djdt_flamegraph.FlamegraphPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
]


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


INTERNAL_IPS = ['127.0.0.1']


SILKY_PYTHON_PROFILER = True
SILKY_PYTHON_PROFILER_BINARY = True
SILKY_PYTHON_PROFILER_RESULT_PATH = '/tmp'

LOG_LEVEL = 'DEBUG'


# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': True,
#     'formatters': {
#         'standard': {
#             'format': '%(asctime)s [%(levelname)s][%(threadName)s]' +
#             '[%(name)s:%(lineno)d] %(message)s'}
#     },
#     'handlers': {
#         'stdout': {
#             'level': 'DEBUG',
#             'class': 'logging.StreamHandler',
#             'stream': sys.stdout,
#             'formatter': 'standard'
#         }
#     },
#     'loggers': {
#         'django': {
#             'handlers': ['stdout'],
#             'level': LOG_LEVEL,
#             'propagate': False
#         },
#         'django.request': {
#             'handlers': ['stdout'],
#             'level': LOG_LEVEL,
#             'propagate': False,
#         },
#         '': {
#             'handlers': ['stdout'],
#             'level': LOG_LEVEL,
#             'propagate': False
#         },
#     }
# }
