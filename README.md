![](./coverage.svg)

[TOC]

# Furion, The Region Manager

This project is based on magnus(IOR), it's purpose is to provide a unitized 
region api. For now there will be three types of regions:

* CASS (in jakiro)
* IOR (in magnus) 
* CLASS (in furion)

## Contribution
1. Add name and email to author list
2. Add change log
3. Run `make` before commit
4. Checkout out a new branch before commit
5. Create a Pull Request before merge (to `master` branch)


### commit message

```
<title>
<why this commit is necessary>
<how do I make it>
<side effects>
```


### docstring
use `google style docstring`

1. [google style docstring](http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)

### Before checkin

Please ensure you follow the code style standard and all the tests passed. It's easier to run the check and test 
in a container environment. eg:

```
docker run -d --restart=always --name=furion -v <your-furion-source-code-dir>:/furion index.alauda.cn/alaudaorg/furion ping www.baidu.com
docker exec -it furion bash
cd /furion
make
```



## ENV Settings

* `LOG_HANDLER="debug,info,error,color"`
* `DB_ENGINE="postgresql"`
* `DB_USER=mathilde`, default is `mathilde`
* `DB_NAME=furiondb`, default is `furiondb`
* `LOG_LEVEL=DEBUG`, default is `DEBUG`
* `LOG_PATH=/var/log/mathilde/`, default is `/var/log/mathilde`
* `DB_HOST="internal-PROD-CN-alauda-DBPool-1527480176.cn-north-1.elb.amazonaws.com.cn"`
* `DB_PASSWORD=""`
* `DB_PORT=5432`
* `DEPLOY_ENV=INT`, default is `CN`, possible values are `[INT,CN]`
* `TINY_ENDPOINT`  Furion will push metrics to tiny,

Example docker run command

```bash
docker run -d --net=host --name=furion \
    -e LOG_LEVEL=DEBUG \
    -e LOG_HANDLER="debug,info,error,color" \
    -e LOG_PATH=/var/log/mathilde/ \
    -e DB_ENGINE="postgresql"  \
    -e DB_USER=mathilde  \
    -e DB_NAME=furiondb  \
    -e DB_HOST="internal-PROD-CN-alauda-DBPool-1527480176.cn-north-1.elb.amazonaws.com.cn" \
    -e DB_PASSWORD="passwordword" \
    -e DB_PORT=5432 \
    -e TINY_ENDPOINT='https://tiny.alauda.cn:8443/v2' \
    -v /var/log/mathilde:/var/log/mathilde \
    index.alauda.cn/mathildedev/furion:release
```



## Migration 

In container, run : 

```bash
python manage.py migrate --noinput
```




## Run
### In container

* INT  `./bin/di.sh`
* CN   `./bin/dc.sh`
* LOCAL `./bin/local.sh`


### In local

`./bin/rl.sh`



## See Also
1. [Design](.docs/design.md)
2. [Region Model](https://bitbucket.org/mathildetech/atropos/wiki/design/claas/region-model.md)
3. [Architecture](./docs/architecture.md)

## Test
### how to run
1. `make test`
2. `python manager.py test tests`
### code structure
1. All test reserved in `tests` folder
2. Well planed is needed