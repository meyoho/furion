#!/usr/bin/python
# -*- coding: utf-8 -*-
from app.control import Control
import logging

from app.serializers import RegionSerializer
from app.models import Region

import copy
from gateway.database import MirrorDBGateway
from gateway.cache import CacheGateway
from app_v2.mirror.core import get_mirror

__author__ = 'Hang Yan'

LOG = logging.getLogger(__name__)


class RegionControl(Control):
    model_name = 'Region'
    model = Region
    serializer = RegionSerializer

    @classmethod
    def _get_list(cls, **args):
        LOG.debug("Get list regions' query params. | {}".format(args))
        ft = {k: args[k][0] for k in cls.model._meta.get_all_field_names() & args.viewkeys()}
        LOG.debug("Formalized list regions query. | {}".format(ft))
        return cls.model.objects.filter(**ft)

    @classmethod
    def _format(cls, obj, duplicated=True):
        new_obj = copy.deepcopy(obj) if duplicated else obj
        data = super(RegionControl, cls)._format(new_obj)
        if not data['mirror']:
            data['mirror'] = {}
            return data
        data['mirror'] = get_mirror(MirrorDBGateway, CacheGateway, data['mirror'])
        return data
