from app.models import Region
from app.models import Mirror
from app_v2.region.entities import RegionData, RegionGetData
from app_v2.mirror.entities import MirrorData
from util.const import KUBERNETES_PLATFORM_VERSION_V4


class DBGateway:

    @staticmethod
    def create_region(region):
        data = region.data.copy()
        data.pop("mirror", "")
        region = Region.objects.create(**data)
        return RegionData(region)

    @staticmethod
    def get_region(region_id):
        region = Region.objects.select_related("mirror").get(id=region_id)
        return RegionGetData(region)

    @staticmethod
    def delete_region(region_id):
        Region.objects.get(id=region_id).delete()

    @staticmethod
    def update_region(region_id, **kwargs):
        if isinstance(region_id, list):
            Region.objects.filter(id__in=region_id).update(**kwargs)
            return
        Region.objects.filter(id=region_id).update(**kwargs)

    @staticmethod
    def list_regions(region_ids):
        if region_ids and region_ids[0]:
            regions = Region.objects.select_related("mirror").filter(id__in=region_ids).order_by("created_at")
        else:
            regions = Region.objects.select_related("mirror").filter(
                platform_version=KUBERNETES_PLATFORM_VERSION_V4).order_by('created_at')
        return [RegionGetData(r) for r in regions]

    @staticmethod
    def get_mirror_regions(mirror_id):
        regions = Region.objects.filter(mirror_id=mirror_id).order_by("created_at")
        return [RegionGetData(r) for r in regions]

    @staticmethod
    def create_mirror(data):
        mirror = Mirror.objects.create(**data)
        return MirrorData(mirror)
    
    @staticmethod
    def update_mirror(mirror_id, **mirror_data):
        Mirror.objects.filter(id=mirror_id).update(**mirror_data)

    @staticmethod
    def get_mirror(mirror_id):
        mirror = Mirror.objects.get(id=mirror_id)
        return MirrorData(mirror), [RegionData(r) for r in mirror.region_set.all()]

    @staticmethod
    def delete_mirror(mirror_id):
        Region.objects.filter(mirror_id=mirror_id).update(**{"mirror_id": None})
        Mirror.objects.get(id=mirror_id).delete()

    @staticmethod
    def list_mirrors(mirror_ids):
        if mirror_ids:
            mirrors = Mirror.objects.filter(id__in=mirror_ids).order_by("created_at")
        else:
            mirrors = Mirror.objects.all().order_by("created_at")
        mirror_data = []
        mirror_regions = {}
        for m in mirrors:
            m_d = MirrorData(m).data
            mirror_data.append(m_d)
            mirror_regions[m_d['id']] = [RegionData(r) for r in m.region_set.all()]
        return mirror_data, mirror_regions

    @staticmethod
    def save_feature_config(region_id, feature_name, config):
        region = Region.objects.get(id=region_id)
        region.features[feature_name] = config
        region.save()
        return config

    @staticmethod
    def get_feature_config(region_id, feature_name):
        region = Region.objects.get(id=region_id)
        return region.features.get(feature_name, {})

    @staticmethod
    def get_feature_config_list(region_id):
        region = Region.objects.get(id=region_id)
        return region.features

    @staticmethod
    def delete_feature_config(region_id, feature_name):
        region = Region.objects.get(id=region_id)
        if feature_name in region.features:
            del region.features[feature_name]
        region.save()


RegionDBGateway = DBGateway
MirrorDBGateway = DBGateway
