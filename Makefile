OS = Linux

VERSION = 0.0.1

CURDIR = $(shell pwd)
SOURCEDIR = $(CURDIR)

ECHO = echo
RM = rm -rf
MKDIR = mkdir

FLAKE8 = flake8
FLAKE8_ARGS = --show-source --statistics --count
FLAKE8_CMD = $(FLAKE8) $(SOURCEDIR) $(FLAKE8_ARGS)
PIP_ARGS = --trusted-host pypi.alauda.io --extra-index-url http://mathildetech:Mathilde1861@pypi.alauda.io/simple/


PIP_INSTALL = sudo pip install

.PHONY: all setup build test help

all: setup build test

setup:
	$(PIP_INSTALL) -q -r requirements-dev.txt
	$(PIP_INSTALL) -q $(PIP_ARGS) -r requirements-alauda.txt

build:
	$(FLAKE8_CMD) --show-pep8 2>/dev/null || $(FLAKE8_CMD)

test:
	coverage run --source="app,cloud,controls,util,views" --omit=*/migrations/*,*/management/*,util/log.py manage.py test tests --setting=furion.settings_ut
	coverage report
	coverage-badge -f -o coverage.svg
help:
	@$(ECHO) "Targets:"
	@$(ECHO) "all     - buildcompile what is necessary"
	@$(ECHO) "setup   - set up prerequisition for build"
	@$(ECHO) "build   - flake8 checks"
	@$(ECHO) "test    - run unittest"
