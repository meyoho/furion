import pytest


@pytest.mark.django_db(transaction=True)
def test_get_region(request, db_with_example_region, api_client, get_region_example, monkeypatch):
    id = request.getfixturevalue("db_with_example_region")
    expected_result = get_region_example
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_version", lambda self: expected_result["attr"]["kubernetes"]["version"])
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.check_namespace_label", lambda self, ns: (False, None))
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.add_namespace_label", lambda self, ns, lb: None)

    r = api_client.get("/v2/regions/{}".format(id))
    for field in ["id", "created_at", "updated_at"]:
        del r.data[field]
        del expected_result[field]
    assert r.data == expected_result


@pytest.mark.django_db(transaction=True)
def test_get_region_with_mirror(request, db_with_example_region_and_mirror, api_client, get_region_with_mirror_example, monkeypatch):
    id = request.getfixturevalue("db_with_example_region_and_mirror")
    expected_result = get_region_with_mirror_example
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_version", lambda self: expected_result["attr"]["kubernetes"]["version"])
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.check_namespace_label", lambda self, ns: (False, None))
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.add_namespace_label", lambda self, ns, lb: None)

    r = api_client.get("/v2/regions/{}".format(id))
    for field in ["id", "created_at", "updated_at"]:
        del r.data[field]
        del expected_result[field]
    del r.data["mirror"]["id"]
    del expected_result["mirror"]["id"]
    for d in r.data["mirror"]["regions"]:
        del d["id"]
    for d in expected_result["mirror"]["regions"]:
        del d["id"]
    for d in r.data["mirror"]["regions"]:
        del d["created_at"]
    assert r.data == expected_result