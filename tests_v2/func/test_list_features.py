import pytest
import uuid

from app.models import Region

from .utils import load_json_data as _json
from .utils import create_region, create_feature, assert_dict_struct_equal


list_feature_examples = {
    "both_log_metric": (
        _json("example_list_features_response_with_log_metric.json"),
        (
          ("log", _json("example_create_log_feature_post.json")),
          ("metric", _json("example_create_metric_feature_post.json"))
        ),
    ),
    "only_log": (
        _json("example_list_features_response_with_log.json"),
        (
          ("log", _json("example_create_log_feature_post.json")),
        )
    ),
}
list_feature_data = [d for _, d in list_feature_examples.items()]
feature_ids = [i for i in list_feature_examples]
valid_region_post_data = _json("example_create_region_post.json")


class Obj:
    pass


class MockApplicationOperator:

    @classmethod
    def get_template(*args, **kwargs):
        return {
            "uuid": "99435da1-4915-4d32-87c2-5e1c984a6689",
            "is_active": True,
            "name": "log",
            "display_name": "official log",
            "description": "",
            "versions": [{
                "uuid":"82435da1-4915-4d32-87c2-5e1c984a6689",
                "values_yaml_content": "the yaml content"
            }]
        }

    @classmethod
    def list_public_templates(*args, **kwargs):
        return {
            "log": {
                "resource_actions": [
                    "public_helm_template_repo:view",
                ],
                "uuid": "99435da1-4915-4d32-87c2-5e1c984a6689",
                "is_active": True,
                "name": "log",
                "display_name": "official log",
                "description": "",
                "icon": "http://icon.com",
                "installed_app_num": 5
            },
            "metric": {
                "resource_actions": [
                    "public_helm_template_repo:view",
                ],
                "uuid": "99435da1-4915-4d32-87c2-5e1c984a6689",
                "is_active": True,
                "name": "metric",
                "display_name": "official metric",
                "description": "",
                "icon": "http://icon.com",
                "installed_app_num": 5
            }
        }

    @classmethod
    def list_application_instances(cls, uuids=None, **kwargs):
        if not uuids:
            return []
        res = []
        for i in uuids:
            d = Obj()
            d.data = {
                "uuid": i,
                "name": "official-log",
                "status": "Running",
                "version": "6465d3e1-7015-4e31-8566-cae5a5725938"
            }
            res.append(d)
        return res

    @classmethod
    def create_template_app(*arg, **kwargs):
        d = Obj()
        d.data = {
            "uuid": str(uuid.uuid4()),
            "name": "official-log",
            "status": "Running"
        }
        return d


class MockIntegrationOperator:

    @classmethod
    def list_integration_instances(cls, uuids=None):
        if not uuids:
            return []
        res = []
        for i in uuids:
            d = Obj()
            d.data = {
                "uuid": i,
                "type": "prometheus",
                "enabled": True
            }
            res.append(d)
        return res


@pytest.fixture(params=list_feature_data, ids=feature_ids)
def list_feature_example(request, api_client, monkeypatch):
    region_id = create_region(api_client, monkeypatch, valid_region_post_data)
    expected_data, feature_create_data = request.param
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_secret_data", lambda *args, **kwargs: {})
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_configmap_data", lambda *args, **kwargs: {})
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_dockercfg_secret", lambda *args, **kwargs: "abcdefg")
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.apply_secret_data", lambda *args, **kwargs: None)
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.apply_configmap_data", lambda *args, **kwargs: None)
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.apply_dockercfg_secret", lambda *args, **kwargs: None)
    monkeypatch.setattr("operators.application.ApplicationOperator.get_template", MockApplicationOperator.get_template)
    monkeypatch.setattr("operators.application.ApplicationOperator.list_public_templates", MockApplicationOperator.list_public_templates)
    monkeypatch.setattr("operators.application.ApplicationOperator.create_template_app", MockApplicationOperator.create_template_app)
    for d in feature_create_data:
        create_feature(api_client, monkeypatch, region_id, d[0], d[1])
    return (region_id, expected_data)


@pytest.mark.django_db(transaction=True)
def test_list_features(request, list_feature_example, api_client, monkeypatch):
    region_id, expected_data = request.getfixturevalue("list_feature_example")
    monkeypatch.setattr("operators.application.ApplicationOperator.list_public_templates", MockApplicationOperator.list_public_templates)
    monkeypatch.setattr("operators.application.ApplicationOperator.get_template", MockApplicationOperator.get_template)
    monkeypatch.setattr("operators.application.ApplicationOperator.list_application_instances", MockApplicationOperator.list_application_instances)
    monkeypatch.setattr("operators.integration.IntegrationOperator.list_integration_instances", MockIntegrationOperator.list_integration_instances)

    r = api_client.get("/v2/regions/{}/features".format(region_id))
    assert_dict_struct_equal(expected_data, r.data)