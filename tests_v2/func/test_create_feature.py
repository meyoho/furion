import pytest
import uuid

from .utils import load_json_data as _json
from .utils import create_region, assert_dict_struct_equal

class Obj:
    pass


create_feature_examples = {
    "valid_log": (
        "log",
        _json("example_create_log_feature_post.json"),
        _json("example_create_log_feature_response.json"),
    ),
    "valid_metric": (
        "metric",
        _json("example_create_metric_feature_post.json"),
        _json("example_create_metric_feature_response.json"),
    ),
    "valid_metric_integration": (
        "metric",
        _json("example_create_metric_integration_feature_post.json"),
        _json("example_create_metric_integration_feature_response.json"),
    )
}
create_feature_data = [d for _, d in create_feature_examples.items()]
feature_ids = [i for i in create_feature_examples]
valid_region_post_data = _json("example_create_region_post.json")

@pytest.fixture(params=create_feature_data, ids=feature_ids)
def create_feature_example(request, api_client, monkeypatch):
    region_id = create_region(api_client, monkeypatch, valid_region_post_data)
    return (region_id,) + request.param


class MockApplicationOperator:

    @classmethod
    def get_template(*args, **kwargs):
        return {
            "uuid": "99435da1-4915-4d32-87c2-5e1c984a6689",
            "is_active": True,
            "name": "log",
            "display_name": "official log",
            "description": "",
            "versions": [{
                "uuid":"82435da1-4915-4d32-87c2-5e1c984a6689",
                "values_yaml_content": "the yaml content"
            }]
        }

    @classmethod
    def list_public_templates(*args, **kwargs):
        return {
            "log": {
                "resource_actions": [
                    "public_helm_template_repo:view",
                ],
                "uuid": "99435da1-4915-4d32-87c2-5e1c984a6689",
                "is_active": True,
                "name": "log",
                "display_name": "official log",
                "description": "",
                "icon": "http://icon.com",
                "installed_app_num": 5
            },
            "metric": {
                "resource_actions": [
                    "public_helm_template_repo:view",
                ],
                "uuid": "99435da1-4915-4d32-87c2-5e1c984a6689",
                "is_active": True,
                "name": "metric",
                "display_name": "official metric",
                "description": "",
                "icon": "http://icon.com",
                "installed_app_num": 5
            }
        }

    @classmethod
    def create_template_app(*arg, **kwargs):
        d = Obj()
        d.data = {
            "uuid": str(uuid.uuid4()),
            "name": "official-log",
            "status": "Running"
        }
        return d


class MockIntegrationOperator:

    @classmethod
    def get_integration_instance(cls, *args, **kwargs):
        d = Obj()
        d.data = {
            "uuid": "91bf91b7-bfdb-4757-8a3b-535b6ad7a25e",
            "type": "prometheus",
            "enabled": True
        }
        return d


@pytest.mark.django_db(transaction=True)
def test_create_feature(request, create_feature_example, api_client, monkeypatch):
    region_id, feature_name, post_data, expected_data = request.getfixturevalue("create_feature_example")
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_secret_data", lambda *args, **kwargs: {})
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_configmap_data", lambda *args, **kwargs: {})
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_dockercfg_secret", lambda *args, **kwargs: "abcdefg")
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.apply_secret_data", lambda *args, **kwargs: None)
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.apply_configmap_data", lambda *args, **kwargs: None)
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.apply_dockercfg_secret", lambda *args, **kwargs: None)
    monkeypatch.setattr("operators.integration.IntegrationOperator.get_integration_instance", MockIntegrationOperator.get_integration_instance)
    monkeypatch.setattr("operators.application.ApplicationOperator.get_template", MockApplicationOperator.get_template)
    monkeypatch.setattr("operators.application.ApplicationOperator.list_public_templates", MockApplicationOperator.list_public_templates)
    monkeypatch.setattr("operators.application.ApplicationOperator.create_template_app", MockApplicationOperator.create_template_app)

    r = api_client.post("/v2/regions/{}/features/{}".format(region_id, feature_name),
                        post_data, format="json")
    assert_dict_struct_equal(expected_data, r.data)
    assert r.data["config"]["type"] == expected_data["config"]["type"]