#!/usr/bin/env bash
SERVER='54.223.32.47:8080'
URL='http://'$SERVER'/v1'

USER='yayu'
APP='app'

# get token 
TOKEN=$(http -b POST $URL/api-token-auth/ username=foo password=bar | jq .token | tr -d '"')
HEADER="Authorization: JWT $TOKEN"

# create region
REGION_ID=$(http -b POST $URL/regions/$USER/ "$HEADER" < ./json/region/aws.json | jq .id | tr -d '"')
echo "Create region. | id=$REGION_ID"

# create cluster
CLUSTER_ID=$(http -b POST $URL/regions/$USER/$REGION_ID/clusters/ "$HEADER" < ./json/cluster/aws.json | jq .id | tr -d '"')
echo "Create cluster. | id=$CLUSTER_ID"

# create single service
SERVICE=$(http -b POST $URL/services/$USER "$HEADER" < ./json/service/create.json | jq .service_name | tr -d '"')
echo "Create service. | name=$SERVICE"

