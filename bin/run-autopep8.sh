#!/bin/sh

/usr/local/bin/autopep8 --verbose --recursive --exclude migrations,settings_docker.py --max-line-length 100 --in-place --aggressive --aggressive $1;

