FROM  index.alauda.cn/alaudaorg/alpine:3.10
LABEL maintainer='hangyan@alauda.io,xdzhang@alauda.io,lgong@alauda.io,yiyuan@alauda.io'

RUN apk update && \
    apk add  linux-pam py-pip && \
    mkdir -p /var/log/mathilde/ && chmod 755 /var/log/mathilde/ && \
    apk add openssh-client git python python-dev libffi-dev postgresql-dev mariadb-dev py-mysqldb libc-dev musl-dev nginx uwsgi wget rsyslog cyrus-sasl-dev uwsgi-python libffi-dev

WORKDIR /furion

RUN python -m pip install --upgrade pip

EXPOSE 8080

ENV TERM xterm
ENV UWSGI_CHEAPER 10
ENV UWSGI_CHEAPER_INITIAL 10

COPY requirements.txt /
COPY . /furion
RUN pip install --no-cache-dir  -r /requirements.txt
RUN pip install --no-cache-dir  -r /furion/requirements-dev.txt
RUN pip install --no-cache-dir --trusted-host pypi.alauda.io --extra-index-url http://mathildetech:Mathilde1861@pypi.alauda.io/simple/ -r /furion/requirements-alauda.txt

RUN mkdir -p /etc/nginx/conf.d/ && echo '' > /etc/nginx/conf.d/default.conf && echo "daemon off;" >> /etc/nginx/nginx.conf && \
    mkdir /run/nginx && \
    rm -rf /etc/supervisord.conf && \
    ln -s /furion/conf/furion_nginx.conf /etc/nginx/conf.d/furion_nginx.conf && \
    ln -s /furion/conf/supervisord.conf /etc/supervisord.conf && \
    chmod a+x /furion/*.sh

CMD ["/furion/run.sh"]
