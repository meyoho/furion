# -*- coding: utf-8 -*-
from rest_framework import serializers

from app.models import Region

__author__ = 'Hang Yan'


class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region


def validate_and_save(serializer):
    if serializer.is_valid(raise_exception=True):
        serializer.save()
        return serializer.data
