from django.conf import settings
from django.conf.urls import patterns, url

from views import region
from alauda.common.web import ping

__author__ = 'Hang Yan'

REGEX = settings.URL_REGEX

urlpatterns = patterns(
    '',
    url(r'^regions/?$', region.RegionsView.as_view(), name='regions/'),
    url(r'^regions/(?P<region_id>{})/?$'.format(REGEX),
        region.RegionView.as_view(),
        name='regions/region-id'),
    # TODO: remove
    url(r'^ping/?$', ping())
)
