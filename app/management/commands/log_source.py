#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
from controls.region import RegionControl
import requests
import json
import os


class Command(BaseCommand):
    help = 'query a region log_source'

    @staticmethod
    def add_arguments(parser):
        parser.add_argument('region_uuid', type=str)

    def handle(self, *args, **options):
        url = "{}/v1/integrations".format(os.getenv('DAVION_ENDPOINT'))
        uuid = options['region_uuid']
        log_source_map = {}

        features = RegionControl.get_object(id=uuid).features

        if features and 'logs' in features and 'storage' in features['logs']:
            write_log_source = features['logs']['storage']['write_log_source']
            read_log_source = features['logs']['storage']['read_log_source']
            log_source = "{},{}".format(write_log_source, read_log_source)
            log_source_uuids = ""

            for uuid in log_source.split(','):
                uuid = uuid.strip()
                if uuid == 'default' or uuid in log_source_map:
                    continue
                if log_source_uuids == "":
                    log_source_uuids = uuid
                else:
                    log_source_uuids += "," + uuid
                log_source_map[uuid] = {}

            rep = requests.get('{}?uuids={}'.format(url, log_source_uuids))
            if rep.status_code != 200:
                print "request failed"

            log_sources = json.loads(rep.text)
            for log_source in log_sources:
                log_source_map[log_source['id']] = log_source['fields']

            ret = {}
            w_array = []
            for w in write_log_source.split(','):
                if w == 'default':
                    continue
                obj = log_source_map[w.strip()]
                obj['id'] = w
                w_array.append(obj)

            r_array = []
            for r in read_log_source.split(','):
                if r == 'default':
                    continue

                obj = log_source_map[r.strip()]
                obj['id'] = r
                r_array.append(obj)

            ret['write_log_source'] = w_array
            ret['read_log_source'] = r_array

            self.stdout.write('-------{}'.format(ret))
