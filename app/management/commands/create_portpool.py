from django.core.management.base import BaseCommand
from app.models import PortPool
from util import const


class Command(BaseCommand):
    help = 'Create port pool. python manage.py ' \
           'create_portpool --public_ip=1.1.1.1 --private_ip=2.2.2.2 --port=7000 --count=50'

    def add_arguments(self, parser):
        parser.add_argument('--private_ip', type=str)
        parser.add_argument('--public_ip', type=str)
        parser.add_argument('--port', type=int, default=7000)
        parser.add_argument('--count', type=int, default=50)

    def handle(self, *args, **options):
        start_port = options.get('port')
        count = options.get('count')
        ssh_range = xrange(start_port, start_port + count)
        mapping_range = xrange(start_port + count, start_port + count * 5)
        PortPool.objects.bulk_create([PortPool(public_ip=options['public_ip'],
                                               private_ip=options['private_ip'],
                                               used=False,
                                               region_name='',
                                               type=const.SSH,
                                               port=port) for port in ssh_range])

        PortPool.objects.bulk_create([PortPool(public_ip=options['public_ip'],
                                               private_ip=options['private_ip'],
                                               used=False,
                                               region_name='',
                                               type=const.MAPPING,
                                               port=port) for port in mapping_range])
