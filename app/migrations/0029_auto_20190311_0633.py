# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0028_auto_20180522_1116'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='componentversion',
            unique_together=set([]),
        ),
        migrations.AlterIndexTogether(
            name='componentversion',
            index_together=set([]),
        ),
        migrations.RemoveField(
            model_name='componentversion',
            name='region_settings',
        ),
        migrations.AlterUniqueTogether(
            name='label',
            unique_together=set([]),
        ),
        migrations.AlterIndexTogether(
            name='label',
            index_together=set([]),
        ),
        migrations.RemoveField(
            model_name='label',
            name='node',
        ),
        migrations.AlterUniqueTogether(
            name='node',
            unique_together=set([]),
        ),
        migrations.AlterIndexTogether(
            name='node',
            index_together=set([]),
        ),
        migrations.RemoveField(
            model_name='node',
            name='region',
        ),
        migrations.DeleteModel(
            name='PortPool',
        ),
        migrations.RemoveField(
            model_name='regionsetting',
            name='region',
        ),
        migrations.AlterUniqueTogether(
            name='stats',
            unique_together=set([]),
        ),
        migrations.AlterIndexTogether(
            name='stats',
            index_together=set([]),
        ),
        migrations.RemoveField(
            model_name='stats',
            name='region',
        ),
        migrations.DeleteModel(
            name='ComponentVersion',
        ),
        migrations.DeleteModel(
            name='Label',
        ),
        migrations.DeleteModel(
            name='Node',
        ),
        migrations.DeleteModel(
            name='RegionSetting',
        ),
        migrations.DeleteModel(
            name='Stats',
        ),
    ]
