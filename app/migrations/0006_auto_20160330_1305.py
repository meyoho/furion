# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_auto_20160322_0930'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='slaveactivity',
            unique_together=set([]),
        ),
        migrations.AlterIndexTogether(
            name='slaveactivity',
            index_together=set([]),
        ),
        migrations.RemoveField(
            model_name='slaveactivity',
            name='region',
        ),
        migrations.AddField(
            model_name='node',
            name='resources',
            field=jsonfield.fields.JSONField(default={}, blank=True),
        ),
        migrations.AlterField(
            model_name='node',
            name='attr',
            field=jsonfield.fields.JSONField(default={}, blank=True),
        ),
        migrations.AlterField(
            model_name='node',
            name='instance_id',
            field=models.CharField(max_length=128, blank=True),
        ),
        migrations.AlterField(
            model_name='node',
            name='state',
            field=models.CharField(default=b'RUNNING', max_length=16, choices=[(b'PENDING', b'Pending'), (b'RUNNING', b'Running'), (b'SHUTTING_DOWN', b'Shutting Down'), (b'STOPPED', b'Stopped'), (b'STOPPING', b'Stopping'), (b'TERMINATED', b'Terminated'), (b'REMOVED', b'Removed'), (b'UNKNOWN', b'Unknown')]),
        ),
        migrations.DeleteModel(
            name='SlaveActivity',
        ),
    ]
