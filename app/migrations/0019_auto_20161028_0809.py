# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0018_auto_20161027_1838'),
    ]

    operations = [
        migrations.AlterField(
            model_name='region',
            name='container_manager',
            field=models.CharField(default=b'NONE', max_length=16, choices=[(b'NONE', b'NONE'), (b'MESOS', b'MESOS'), (b'LEGACY', b'LEGACY')]),
        ),
    ]
