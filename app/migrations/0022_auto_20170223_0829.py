# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0021_auto_20170223_0655'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stats',
            name='type',
            field=models.CharField(max_length=32, choices=[(b'MESOS_METRICS', b'Mesos Metrics'), (b'SWARM_METRICS', b'Swarm Metrics'), (b'KUBERNETES_METRICS', b'Kubernetes Metrics')]),
        ),
    ]
