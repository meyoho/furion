# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0022_auto_20170223_0829'),
    ]

    operations = [
        migrations.AddField(
            model_name='portpool',
            name='namespace',
            field=models.CharField(default='', max_length=36, blank=True),
        ),
    ]
