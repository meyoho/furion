# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0020_auto_20170216_0358'),
    ]

    operations = [
        migrations.AlterField(
            model_name='node',
            name='type',
            field=models.CharField(default=b'SLAVE', max_length=16, choices=[(b'SLAVE', b'Slave'), (b'SYS', b'Sys'), (b'EMPTY', b'Empty'), (b'SYS', b'Sys'), (b'SLAVE', b'Slave'), (b'SYS', b'Sys'), (b'SLAVE', b'Slave')]),
        ),
        migrations.AlterField(
            model_name='region',
            name='container_manager',
            field=models.CharField(default=b'NONE', max_length=16, choices=[(b'NONE', b'NONE'), (b'MESOS', b'MESOS'), (b'SWARM', b'SWARM'), (b'LEGACY', b'LEGACY'), (b'KUBERNETES', b'Kubernetes')]),
        ),
        migrations.AlterField(
            model_name='stats',
            name='type',
            field=models.CharField(max_length=16, choices=[(b'MESOS_METRICS', b'Mesos Metrics'), (b'SWARM_METRICS', b'Swarm Metrics'), (b'KUBERNETES_METRICS', b'Kubernetes Metrics')]),
        ),
    ]
