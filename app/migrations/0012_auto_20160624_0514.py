# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0011_auto_20160602_0407'),
    ]

    operations = [
        migrations.AlterField(
            model_name='node',
            name='updated_at',
            field=models.DateTimeField(),
        ),
    ]
