# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0016_auto_20161001_1631'),
    ]

    operations = [
        migrations.AlterField(
            model_name='region',
            name='container_manager',
            field=models.CharField(default=b'EMPTY', max_length=16, choices=[(b'EMPTY', b'EMPTY'), (b'ACMP', b'ACMP'), (b'LEGACY', b'LEGACY')]),
        ),
    ]
