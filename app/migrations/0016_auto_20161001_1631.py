# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0015_auto_20160826_0539'),
    ]

    operations = [
        migrations.AddField(
            model_name='region',
            name='container_manager',
            field=models.CharField(default=b'EMPTY', max_length=16, choices=[(b'EMPTY', b'EMPTY'), (b'ACMP', b'ACMP')]),
        ),
        migrations.AlterField(
            model_name='node',
            name='type',
            field=models.CharField(default=b'SLAVE', max_length=16, choices=[(b'SLAVE', b'Slave'), (b'SYS', b'Sys'), (b'EMPTY', b'Empty')]),
        ),
        migrations.AlterField(
            model_name='regionsetting',
            name='user_registries',
            field=jsonfield.fields.JSONField(default=dict, blank=True),
        ),
        migrations.AlterField(
            model_name='regionsetting',
            name='user_settings',
            field=jsonfield.fields.JSONField(default=dict, blank=True),
        ),
    ]
