# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0025_auto_20170827_1342'),
    ]

    operations = [
        migrations.AddField(
            model_name='region',
            name='version',
            field=models.CharField(default='v2', max_length=16),
        ),
    ]
