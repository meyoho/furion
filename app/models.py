from __future__ import unicode_literals

import uuid

import jsonfield
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from util import const


def gen_uuid():
    return str(uuid.uuid4())


class Time(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Base(Time):
    id = models.CharField(max_length=36, primary_key=True, default=gen_uuid)

    class Meta:
        abstract = True


class Mirror(Base):
    name = models.CharField(max_length=36)
    display_name = models.CharField(max_length=36)
    description = models.CharField(max_length=1024, default='')
    flag = models.CharField(max_length=36)


@python_2_unicode_compatible
class Region(Base):
    name = models.CharField(max_length=36)
    mirror = models.ForeignKey(Mirror, null=True)
    display_name = models.CharField(max_length=36)
    namespace = models.CharField(max_length=36, blank=True, default='')
    state = models.CharField(max_length=16, choices=const.REGION_STATE, default=const.STATE_RUNNING)
    platform_version = models.CharField(max_length=16, default=const.KUBERNETES_PLATFORM_VERSION_V2, null=True)
    container_manager = models.CharField(max_length=16,
                                         choices=const.CONTAINER_MANAGER,
                                         default=const.CONTAINER_MANAGER_NONE)
    attr = jsonfield.JSONField()
    features = jsonfield.JSONField()
    env_uuid = models.CharField(max_length=36, null=True, db_index=True)

    class Meta:
        unique_together = ('namespace', 'name')
        index_together = ('namespace', 'name')

    def __str__(self):
        return '{} {}'.format(self.namespace, self.name)

    def is_swarm_region(self):
        return self.container_manager == const.CONTAINER_MANAGER_SWARM
