#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

__author__ = 'Hang Yan'

LOG = logging.getLogger(__name__)


def _new_attr(region, comp_name, attr):
    return region.attr.get('components', {}).get(comp_name, {}).get(attr)


def right_attr(region, comp_name, attr, old_value=''):
    new_value = _new_attr(region, comp_name, attr)
    return new_value if new_value else old_value
