# flake8: noqa
import sys
import os
import django
from util.func import wrapped_decode_aes
from alauda.common.func import pad_str
from django.conf import settings
import click
from Crypto.Cipher import AES
import time


def backup(region, time, status):
    with open(".{}_{}_{}.{}".format(region.namespace, region.name, time, status), "wb+") as f:
        f.write("attr: {}\n".format(region.attr))
        f.write("features: {}\n".format(region.features))
        f.write("platform_version: {}\n".format(region.platform_version))
        f.write("env_uuid: {}\n".format(region.env_uuid))


def transform(region):
    current_time = time.time()
    backup(region, current_time, "old")
    cipher = AES.new(pad_str(settings.SECRET_KEY)[:32])
    print "Upgrade region: {}".format(region.name)
    v4_attr = {}
    v4_features = {}
    v4_attr_cloud = {}
    v4_attr_docker = {}
    v4_attr_cluster = {}
    v4_attr_kubernetes = {}
    v4_attr_cloud['name'] = region.attr['cloud']['name']
    v4_attr_docker['path'] = region.attr['docker']['path']
    v4_attr_docker['version'] = region.attr['docker']['version']
    v4_attr_cluster['nic'] = region.attr['nic']
    v4_attr_kubernetes['cni'] = {}
    v4_attr_kubernetes['cni']['cidr'] = region.features['kubernetes']['cni']['cidr']
    v4_attr_kubernetes['cni']['type'] = region.features['kubernetes']['cni']['type']
    v4_attr_kubernetes['cni']['backend'] = region.features['kubernetes']['cni']['backend']
    v4_attr_kubernetes['cni']['network_policy'] = region.features['kubernetes']['cni']['network_policy']
    v4_attr_kubernetes['type'] = 'original'
    v4_attr_kubernetes['token'] = wrapped_decode_aes(cipher, region.features['service']['manager']['token'])
    v4_attr_kubernetes['version'] = region.features['kubernetes']['version']
    v4_attr_kubernetes['endpoint'] = region.features['service']['manager']['endpoint']

    v4_attr['cloud'] = v4_attr_cloud
    v4_attr['cluster'] = v4_attr_cluster
    v4_attr['docker'] = v4_attr_docker
    v4_attr['kubernetes'] = v4_attr_kubernetes
    v4_attr['feature_namespace'] = 'default'

    region.attr = v4_attr
    region.features = v4_features
    region.platform_version = "v4"
    region.env_uuid = ""
    region.save()

    backup(region, current_time, "new")

    print "Finish Upgrade region: {}".format(region.name)


@click.command(short_help="Upgrade platform version from v3 to v4",
               context_settings=dict(ignore_unknown_options=True, allow_extra_args=True))
@click.option("--name", "-n", help="The name of the region")
@click.option("--all", "-a", help="Upgrade all regions")
@click.pass_context
def upgrade(ctx, name, all):
    if (not name) and (not all):
        print "Please specify name or specify all flag!"
        sys.exit(1)
    if name:
        region = Region.objects.get(name=name, platform_version="v3")
        transform(region)
    elif all == 'true':
        print "Upgrade all regions"
        regions = Region.objects.filter(platform_version="v3")
        for region in regions:
            transform(region)


if __name__ == "__main__":
    sys.path.append('/furion')
    os.environ['DJANGO_SETTINGS_MODULE'] = 'furion.settings'
    django.setup()
    from app.models import Region
    upgrade()
