#!/usr/bin/env bash

cd "$(dirname "$0")"

env | grep -v LS_COLORS | sed 's/=\(.*\)/="\1"/' | sed 's/^/export /g' > /share_env.var
python manage.py crontab add
/usr/bin/supervisord --nodaemon

python manage.py crontab remove
