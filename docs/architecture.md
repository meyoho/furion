[TOC]

# Architecture



![](arc.png)



## Puck

Periodically retrieve region information from marathon's info api and send back to jakiro(then to furion)



1.  Built-in cron job in puck image
2.  Get zk info from marathon's info api
3.  Parse mesos master info from zk 
4.  Get mesos slave info and cluster resource usage from mesos api
5.  Get apps info from marathon app api (sven cache)
6.  Send all the data to jakiro



## Datadog Agent

Agent mainly for gather infomation about components(for now)



*   For Mesos,Marathon,Puck, check puck's http endpoint
*   For haproxy, tiny(ES), check http endpoint.
*   For Meepo / Doom, check tcp endpoint
*   For Mesos-slave, check if container exists for zeus/nevermore/docker-proxy

All the datadog agents will send the metrics info to naix every 15 secs.



## Naix

Parse the metric info from datadog agent and get the state of components, then send it to jakiro (update node api)

*   Parse slave's componets from containers list's metrics
*   Parse other sys componets from corresponding metrics (`service_checks`)
*   Parse other meta info from `alauda.env`



## Jakiro

All the metrics data from naix or puck will send to jakiro ( do auth by user token). Jakiro will send most of them to Furion(send apps cache to sven)



*   `PUT /regions/<namespace>/<region_name>/stats` for data from puck
*   `PUT /regions/<namespace>/<region_name>/<private_ip>` for data from naix



## Furion

Furion will update nodes info and current stats info for region when it receive new metrics data from jakiro. It store the current state of a region, and then it will send the metrics data to tiny, so tiny have the data's history.



## Tiny

Store the mtircs info of region/node/componets, jakiro will directly call tiny to get these data.





























