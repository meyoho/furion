# Project Structure

* `app/` main django app for regions
* `bin/` various bash script for deploy and test
* `conf/` configure files for docker
* `controls/` controls for various resources(regions, clusters...)
* `furion/` project settings
* `json/` example api data
* `util/` utility functions
* `views/` views for various resources(regions, clusters...)


# Implementation Details

## Log
### Request log
For every api request, there is a middleware to record the corresponding information
(method, url, status code...)

### Color
There is an extra file to store the debug logs ,but with color escape chars. It's useful
when debug the program in console (use tail)

### Sentences
Log sentences should be separated into two parts: description and data. The 
description describes 
what happened and data records the context data. For this purpose, there is a custom log 
class to support split them with a `|` and will show them in different color.


## Exception

Use Exception or error code is an insignificant choice, but should be consistency in one project.
We have create a `FurionException` to handle all the error conditions.


## Abstract Control
Most control module are similar, so we add an abstract control module which implement many common
operations on mode, such as list/get/update.... For every resource, it should subclass the abstract 
control model and custom it's behaviour.











